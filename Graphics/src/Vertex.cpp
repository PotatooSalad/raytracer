#include "Vertex.h"

std::ostream& operator<<(std::ostream& out, Vertex v)
{
	out << "pos: " << v.position << " norm: " << v.normal << " tex: " << v.texCoord << std::endl;
	return out;
}