#include "Texture.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "PixelColor.h"
#include "Vector3.h"
#include "Vector2.h"
#define _USE_MATH_DEFINES
#include <math.h>


Texture::Texture(const char* filename, uint desiredChannels)
{
	int width, height, channels;
	colorData = stbi_load(filename, &width, &height, &channels, desiredChannels);
	textureHeight = height;
	textureWidth = width;
}

Texture::~Texture()
{
	memset(colorData, 0, textureWidth * textureHeight * 3);
	delete[] colorData;
}

PixelColor Texture::getPixelColor(const uint& x, const uint& y)
{
	byte r, g, b;
	r = colorData[y * textureWidth * 3 + x * 3 + 0];
	g = colorData[y * textureWidth * 3 + x * 3 + 1];
	b = colorData[y * textureWidth * 3 + x * 3 + 2];
	return PixelColor(r, g, b);
}

uint Texture::getWidth()
{
	return textureWidth;
}

uint Texture::getHeight()
{
	return textureHeight;
}

PixelColor Texture::extractTextureColor(Vector2& uv)
{
	int x = (int)((textureWidth - 1) * uv.getX());
	int y = (int)((textureHeight - 1) * uv.getY());
	while (x > textureWidth - 1)
	{
		x -= textureWidth - 1;
	}
	while (y > textureHeight - 1)
	{
		y -= textureHeight - 1;
	}
	while (x < 0)
	{
		x += textureWidth - 1;
	}
	while (y < 0)
	{
		y += textureHeight - 1;
	}
	return getPixelColor(x, y);
}
