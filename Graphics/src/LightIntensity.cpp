#include "LightIntensity.h"
#include "PixelColor.h"
#include "Vector3.h"

#include <algorithm>

LightIntensity::LightIntensity()
{
	red = green = blue = 0.0;
}

LightIntensity::LightIntensity(PixelColor color)
{
	red = color.getRed() / 255.0;
	green = color.getGreen() / 255.0;
	blue = color.getBlue() / 255.0;
	clampValues();
}

LightIntensity::LightIntensity(double r, double g, double b) : red(r), green(g), blue(b)
{
	//clampValues();
}

LightIntensity::LightIntensity(Vector3 v)
{
	this->red = v.getX();
	this->green = v.getY();
	this->blue = v.getZ();
	clampValues();
}

LightIntensity::LightIntensity(double r, double g) : red(r), green(g)
{
	blue = 0.0;
	//clampValues();
}

LightIntensity::LightIntensity(double r) : red(r)
{
	green = blue = 0.0;
	//clampValues();
}

void LightIntensity::clampValues()
{
	red = std::clamp(red, 0.0, 1.0);
	blue = std::clamp(blue, 0.0, 1.0);
	green = std::clamp(green, 0.0, 1.0);
}

LightIntensity LightIntensity::operator+(const LightIntensity& addend) const
{
	return LightIntensity(
		this->red + addend.red, 
		this->green + addend.green, 
		this->blue + addend.blue
	);
}

LightIntensity LightIntensity::operator-(const LightIntensity& subtrahend) const
{
	return LightIntensity(
		this->red - subtrahend.red,
		this->green - subtrahend.green,
		this->blue - subtrahend.blue
	);
}

LightIntensity LightIntensity::operator*(const float& factor) const
{
	return LightIntensity(
		this->red * factor,
		this->green * factor,
		this->blue * factor
	);
}

LightIntensity LightIntensity::operator*(const LightIntensity& factor) const
{
	return LightIntensity(
		this->red * factor.red,
		this->green * factor.green,
		this->blue * factor.blue
	);
}

LightIntensity LightIntensity::operator/(const float& divisor) const
{
	return LightIntensity(
		this->red / divisor,
		this->green / divisor,
		this->blue / divisor
	);
}

LightIntensity& LightIntensity::operator+=(const LightIntensity& addend)
{
	this->red += addend.red;
	this->green += addend.green;
	this->blue += addend.blue;
	//clampValues();
	return *this;
}

LightIntensity& LightIntensity::operator-=(const LightIntensity& subtrahend)
{
	this->red -= subtrahend.red;
	this->green -= subtrahend.green;
	this->blue -= subtrahend.blue;
	clampValues();
	return *this;
}

LightIntensity& LightIntensity::operator*=(const float factor)
{
	this->red *= factor;
	this->green *= factor;
	this->blue *= factor;
	clampValues();
	return *this;
}

LightIntensity& LightIntensity::operator/=(const float divisor)
{
	this->red /= divisor;
	this->green /= divisor;
	this->blue /= divisor;
	clampValues();
	return *this;
}

bool LightIntensity::operator==(LightIntensity& other) const
{
	return abs(red - other.red) < 0.0001 &&
		abs(blue - other.blue) < 0.0001 &&
		abs(green - other.green) < 0.0001;
}

bool LightIntensity::operator!=(LightIntensity& other) const
{
	return abs(red - other.red) > 0.0001 ||
		abs(blue - other.blue) > 0.0001 ||
		abs(green - other.green) > 0.0001;
}

LightIntensity operator*(const float& factor, const LightIntensity& lFactor)
{
	return lFactor * factor;
}

std::ostream& operator<<(std::ostream& out, LightIntensity& intensity)
{
	out << "rgb = (" << intensity.getRed() << ", " << intensity.getGreen() << ", " << intensity.getBlue() << ")";
	return out;
}