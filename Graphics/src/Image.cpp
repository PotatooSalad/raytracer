#include "Image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#include "PixelColor.h"

Image::Image(uint width, uint height)
{
	this->width = width;
	this->height = height;
	colorData = new byte[width * height * 3];
	depthData = new float[width * height];
	stbi_write_png_compression_level = 0;
}
Image::~Image()
{
	memset(colorData, 0, width * height * 3);
	memset(depthData, 0.0f, width * height);
	delete[] colorData, depthData;
}

void Image::saveToBmp(const char* filename)
{
	stbi_write_bmp(filename, width, height, 3, colorData);
}

void Image::saveToPng(const char* filename)
{
	stbi_write_png(filename, width, height, 3, colorData, width * 3);
}

void Image::saveToTga(const char* filename)
{
	stbi_write_tga(filename, width, height, 3, colorData);
}

void Image::clearColor(const PixelColor& color)
{
	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			colorData[y * width * 3 + x * 3 + 0] = color.getRed();
			colorData[y * width * 3 + x * 3 + 1] = color.getGreen();
			colorData[y * width * 3 + x * 3 + 2] = color.getBlue();
		}
	}
}

void Image::clearDepth(const float value)
{
	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			depthData[y * width + x] = value;
		}
	}
}

void Image::setPixelColor(const uint& x, const uint& y, const PixelColor& color)
{
	colorData[y * width * 3 + x * 3 + 0] = color.getRed();
	colorData[y * width * 3 + x * 3 + 1] = color.getGreen();
	colorData[y * width * 3 + x * 3 + 2] = color.getBlue();
}

PixelColor Image::getPixelColor(const uint& x, const uint& y)
{
	byte r, g, b;
	r = colorData[y * width * 3 + x * 3 + 0];
	g = colorData[y * width * 3 + x * 3 + 1];
	b = colorData[y * width * 3 + x * 3 + 2];
	return PixelColor(r, g, b);
}

void Image::setPixelDepth(const uint& x, const uint& y, const float& depthValue)
{
	depthData[y * width + x] = depthValue;
}

float Image::getPixelDepth(const uint& x, const uint& y) const
{
	return depthData[y + width + x];
}

uint Image::getWidth()
{
	return width;
}

uint Image::getHeight()
{
	return height;
}
