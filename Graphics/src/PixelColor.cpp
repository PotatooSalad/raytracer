#include "PixelColor.h"
#include "LightIntensity.h"
#include <algorithm>

PixelColor::PixelColor(LightIntensity light)
{
	light.clampValues();
	this->r = byte(light.getRed() * 255.0);
	this->g = byte(light.getGreen() * 255.0);
	this->b = byte(light.getBlue() * 255.0);
}

bool PixelColor::operator!=(PixelColor& other) const
{
	return this->r != other.r || this->g != other.g || this->b != other.b;
}

bool PixelColor::operator==(PixelColor& other) const
{
	return this->r == other.r && this->b == other.b && this->g == other.g;
}

PixelColor PixelColor::operator+(PixelColor addend) const
{
	int r, g, b;
	r = (int)this->r + addend.r;
	g = (int)this->g + addend.g;
	b = (int)this->b + addend.b;
	return PixelColor(
		std::clamp(r, 0, 255),
		std::clamp(g, 0, 255),
		std::clamp(b, 0, 255)
	);
}

PixelColor PixelColor::operator*(double factor) const
{
	double r, g, b;
	r = this->r * factor;
	g = this->g * factor;
	b = this->b * factor;
	return PixelColor(
		(byte)std::clamp(r, 0.0, 255.0),
		(byte)std::clamp(g, 0.0, 255.0),
		(byte)std::clamp(b, 0.0, 255.0)
	);
}