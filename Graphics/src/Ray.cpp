#include "Ray.h"

Ray::Ray()
{
	origin = Vector3::zero;
	direction = Vector3::zero;
	destination = Vector3::zero;
}


Ray::Ray(Vector3 newOrigin, Vector3 newDirection)
{
	origin = newOrigin;
	direction = newDirection.normalized();
	destination = Vector3::zero;
}

Vector3 Ray::getOrigin()
{
	return origin;
}

Vector3 Ray::getDirection()
{
	return direction;
}

Vector3 Ray::getDestination()
{
	return destination;
}

void Ray::setOrigin(Vector3 newOrigin)
{
	origin = newOrigin;
}

void Ray::setDirection(Vector3 newDirection)
{
	direction = newDirection;
	direction.normalize();
}

void Ray::setDestination(Vector3 newDestination)
{
	destination = newDestination;
	if (!isFloatZero(destination.getLength()))
	{
		setDirection(destination.normalized() - getOrigin());
	}
	else
	{
		setDirection(-getOrigin());
	}
	
}

Vector3 Ray::pointAtDistance(float distance)
{
	return origin + distance * direction;
}
