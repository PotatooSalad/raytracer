#pragma once
#include "Vector3.h"
#include "Vector2.h"

/// <summary>
/// Vertex structure. Contains position, normal and texture coordinates
/// </summary>
struct Vertex
{
	/// <summary>
	/// Position field
	/// </summary>
	Vector3 position;

	/// <summary>
	/// Vertex normal field
	/// </summary>
	Vector3 normal;

	/// <summary>
	/// Vertex texture coordinate field
	/// </summary>
	Vector2 texCoord;

	/// <summary>
	/// Basic constructor of Vertex, with position only
	/// </summary>
	/// <param name="position">Position data</param>
	Vertex(Vector3 position) : position(position) {}

	/// <summary>
	/// Constructor which assigns all three struct fields
	/// </summary>
	/// <param name="position">Value of position field</param>
	/// <param name="normal">Value of normal field</param>
	/// <param name="texCoord">Value of texture coord field</param>
	Vertex(Vector3 position, Vector3 normal, Vector2 texCoord) : position(position), normal(normal), texCoord(texCoord) {}

	/// <summary>
	/// Default constructor of Vertex
	/// </summary>
	Vertex() = default;
};

/// <summary>
/// Overloaded operator for Vertex class (aka "toString")
/// </summary>
/// <param name="out">Handle to ostream object</param>
/// <param name="vector">Vertex which will be made to string</param>
/// <returns> Handle to output stream with string Vertex data </returns>
std::ostream& operator<<(std::ostream& out, Vertex v);