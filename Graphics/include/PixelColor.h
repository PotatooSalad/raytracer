#pragma once
#include "globals.h"

class LightIntensity;

/// <summary>
/// Pixel color class
/// </summary>
class PixelColor
{
public:
	/// <summary>
	/// Pixel Color class constructor with three parameters: red, green and blue
	/// </summary>
	/// <param name="red"></param>
	/// <param name="green"></param>
	/// <param name="blue"></param>
	PixelColor(byte red, byte green, byte blue) : r(red), g(green), b(blue) {}

	/// <summary>
	/// Pixel color class constructor which sets all three values to the same value, making pixel grayish
	/// </summary>
	/// <param name="gray">Gray color intensity value</param>
	PixelColor(byte gray) : r(gray), g(gray), b(gray) {}

	/// <summary>
	/// LightIntensity to PixelColor mapper constructor
	/// </summary>
	/// <param name="light">LightIntensity color value</param>
	PixelColor(LightIntensity light);

	/// <summary>
	/// Red component value getter
	/// </summary>
	/// <returns>Value of red color component</returns>
	byte getRed() const { return r; }
	/// <summary>
	/// Green component value getter
	/// </summary>
	/// <returns>Value of green color component</returns>
	byte getGreen() const { return g; }
	/// <summary>
	/// Blue component value getter
	/// </summary>
	/// <returns>Value of blue color component</returns>
	byte getBlue() const { return b; }

	/// <summary>
	/// Inequality operator for pixel colors
	/// </summary>
	/// <param name="other">Other pixel color to compare</param>
	/// <returns>True: if colors are not equal</returns>
	bool operator!=(PixelColor& other) const;

	/// <summary>
	/// Equality operator for pixel colors
	/// </summary>
	/// <param name="other">Other pixel color to compare</param>
	/// <returns></returns>
	bool operator==(PixelColor& other) const;

	/// <summary>
	/// Addition operator of two PixelColors
	/// </summary>
	/// <param name="addend">Addend of operation</param>
	/// <returns>New PixelColor object which is sum of this and addend</returns>
	PixelColor operator+(PixelColor addend) const;

	/// <summary>
	/// Multiplication operator of Pixel color and floating-point value
	/// </summary>
	/// <param name="factor">Factor of operation</param>
	/// <returns>New PixelColor object which is product of this and factor</returns>
	PixelColor operator*(double factor) const;

private:
	/// <summary>
	/// Red component of color
	/// </summary>
	byte r;
	/// <summary>
	/// Green component of color
	/// </summary>
	byte g;
	/// <summary>
	/// Blue component of color
	/// </summary>
	byte b;
};