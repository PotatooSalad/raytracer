#pragma once
#include "globals.h"
class PixelColor;

/// <summary>
/// Image, a.k.a. screen buffer class
/// </summary>
class Image
{
public:
	/// <summary>
	/// Constructor of Image (Screen buffer).
	/// </summary>
	/// <param name="width">Width of image (buffer) in pixels</param>
	/// <param name="height">Height of image (buffer) in pixels</param>
	Image(uint width, uint height);

	/// <summary>
	/// Destructor of Image (Screen buffer).
	/// </summary>
	~Image();

	/// <summary>
	/// Saves color buffer to BMP file
	/// </summary>
	/// <param name="filename">Name (might be with relative path) of file to save</param>
	void saveToBmp(const char* filename);

	/// <summary>
	/// Saves color buffer to PNG file
	/// </summary>
	/// <param name="filename">Name (might be with relative path) of file to save</param>
	void saveToPng(const char* filename);

	/// <summary>
	/// Saves color buffer to TGA file
	/// </summary>
	/// <param name="filename">Name (might be with relative path) of file to save</param>
	void saveToTga(const char* filename);

	/// <summary>
	/// Sets color of pixel in color buffer to given color value
	/// </summary>
	/// <param name="x">X coordinate of pixel</param>
	/// <param name="y">Y coordinate of pixel</param>
	/// <param name="color">Color to set</param>
	void setPixelColor(const uint& x, const uint& y, const PixelColor& color);

	/// <summary>
	/// Gets color of selected pixel
	/// </summary>
	/// <param name="x">X coordinate of pixel</param>
	/// <param name="y">Y coordinate of pixel</param>
	/// <returns>Color object of selected pixel which was read from color buffer</returns>
	PixelColor getPixelColor(const uint& x, const uint& y);

	/// <summary>
	/// Sets depth value of pixel in depth buffer to given value
	/// </summary>
	/// <param name="x">X coordinate of pixel</param>
	/// <param name="y">Y coordinate of pixel</param>
	/// <param name="depthValue">Depth value to set</param>
	void setPixelDepth(const uint& x, const uint& y, const float& depthValue);

	/// <summary>
	/// Gets pixel depth value
	/// </summary>
	/// <param name="x">X coordinate of pixel</param>
	/// <param name="y">Y coordinate of pixel</param>
	/// <returns>Depth value of selected pixel</returns>
	float getPixelDepth(const uint& x, const uint& y) const;

	/// <summary>
	/// Gets width of the Image
	/// </summary>
	/// <returns>Unsigned int value of width</returns>
	uint getWidth();

	/// <summary>
	/// Gets height of the Image
	/// </summary>
	/// <returns>Unsigned int value of height</returns>
	uint getHeight();

	/// <summary>
	/// Sets all color buffer with one given color
	/// </summary>
	/// <param name="color">Color to set</param>
	void clearColor(const PixelColor& color);

	/// <summary>
	/// Sets all depth values in depth buffer to given
	/// </summary>
	/// <param name="value">Depth value to set</param>
	void clearDepth(const float value);
private:
	/// <summary>
	/// Width of image (buffer) in pixels
	/// </summary>
	uint width;
	/// <summary>
	/// Height of image (buffer) in pixels
	/// </summary>
	uint height;
	/// <summary>
	/// Color buffer; In constructor it is initialized with size (width * height * 3).
	/// </summary>
	byte* colorData = nullptr;
	/// <summary>
	/// Depth buffer; In constructor it is initialized with size (width * height).
	/// </summary>
	float* depthData = nullptr;
};

