#pragma once
#include "globals.h"
class PixelColor;
class Vector3;
class Vector2;

/// <summary>
/// Texture class
/// </summary>
class Texture
{
public:
	/// <summary>
	/// Constructor for Texture that loads texture from file. Currently working: .tga .png
	/// </summary>
	/// <param name="filename">Path to the texture</param>
	/// <param name="desiredChannels">Desired channels of Texture</param>
	Texture(const char* filename, uint desiredChannels);

	/// <summary>
	/// Destructor of Texture
	/// </summary>
	~Texture();

	/// <summary>
	/// Getter of PixelColor in X, Y coordinates
	/// </summary>
	/// <param name="x">Coordinate X</param>
	/// <param name="y">Coordinate X</param>
	/// <returns>PixelColor in X, Y coordinates</returns>
	PixelColor getPixelColor(const uint& x, const uint& y);

	/// <summary>
	/// Getter of texture width
	/// </summary>
	/// <returns>Uint value of width</returns>
	uint getWidth();

	/// <summary>
	/// Getter of texture height
	/// </summary>
	/// <returns>uint value of height</returns>
	uint getHeight();

	/// <summary>
	/// Getter of PixelColor in UV coordinates
	/// </summary>
	/// <param name="uv">Vector2 that contains UV coordinates</param>
	/// <returns>PixelColor value in UV coordinates</returns>
	PixelColor extractTextureColor(Vector2& uv);	

private:
	/// <summary>
	/// Uint value of Texture Width
	/// </summary>
	uint textureWidth;

	/// <summary>
	/// Uint value of Texture Height
	/// </summary>
	uint textureHeight;

	/// <summary>
	/// Color buffer; In constructor it is initialized with size (width * height * 3).
	/// </summary>
	byte* colorData = nullptr;
};