#pragma once

#include <iostream>
#include "Vector3.h"
#include "globals.h"

/// <summary>
/// Implementation of Ray
/// </summary>
class Ray
{
public:

	/// <summary>
	/// Default constructor for Ray
	/// </summary>
	Ray();

	/// <summary>
	/// Constructor for Ray
	/// </summary>
	/// <param name="newOrigin">Vector3 value assigned to origin</param>
	/// <param name="newDirection">Vector3 value assigned to direction</param>
	Ray(Vector3 newOrigin, Vector3 newDirection);

	/// <summary>
	/// Getter of origin value of Ray
	/// </summary>
	/// <returns>Vector3 value of origin</returns>
	Vector3 getOrigin();

	/// <summary>
	/// Getter of direction value of Ray
	/// </summary>
	/// <returns>Vector3 value of direction</returns>
	Vector3 getDirection();

	/// <summary>
	/// Getter of destination value of Ray
	/// </summary>
	/// <returns>Vector3 value of destination</returns>
	Vector3 getDestination();

	/// <summary>
	/// Setter of Vector3 value origin and modifies Destination
	/// using pointAtDistance function
	/// </summary>
	/// <param name="newOrigin">New value of origin</param>
	void setOrigin(Vector3 newOrigin);

	/// <summary>
	/// Setter of Vector3 value direction and modifies Destination
	/// using pointAtDistance function
	/// </summary>
	/// <param name="newDirection">New value of direction</param>
	void setDirection(Vector3 newDirection);

	/// <summary>
	/// Setter of Vector3 value destination
	/// </summary>
	/// <param name="newDestination">New value of destination</param>
	void setDestination(Vector3 newDestination);

	/// <summary>
	/// Get point at distance from ray
	/// </summary>
	/// <param name="distance">Distance from ray's origin</param>
	/// <returns>point shiftd from origin by distance</returns>
	Vector3 pointAtDistance(float distance);
private:
	/// <summary>
	/// Origin of ray
	/// </summary>
	Vector3 origin;
	/// <summary>
	/// Ray direction
	/// </summary>
	Vector3 direction;
	/// <summary>
	/// Point which with ray intersects (it lays on ray's route)
	/// </summary>
	Vector3 destination;
};