#pragma once
#include "LightIntensity.h"
#include <string>

class Texture;

/// <summary>
/// Enum that describes material type
/// </summary>
enum class MaterialType : unsigned int
{
	NORMAL,
	MIRROR,
	GLASS,
	NONE
};

/// <summary>
/// Material class
/// </summary>
class Material
{
public:
	/// <summary>
	/// Material object constructor
	/// </summary>
	/// <param name="name">Name of material</param>
	/// <param name="diffuse">Diffuse color</param>
	/// <param name="specular">Specular factor</param>
	/// <param name="shineness">Shineness factor</param>
	/// <param name="mt">type of material (default NORMAL)</param>
	/// <param name="refractIndex">Index of refraction (default 1.0)</param>
	Material(std::string name, LightIntensity diffuse, float specular, float shineness = 2.0f, MaterialType mt = MaterialType::NORMAL, float refractIndex = 1.0f)
		: name(name), ambient(diffuse * 0.2f), diffuse(diffuse), specular(specular), shineness(shineness), materialType(mt), indexOfRefraction(refractIndex) {}

	/// <summary>
	/// Diffuse color getter
	/// </summary>
	/// <returns>Value of color in diffuse field</returns>
	LightIntensity getDiffuse() const { return diffuse; }

	/// <summary>
	/// Ambient color getter
	/// </summary>
	/// <returns>Value of ambient color</returns>
	LightIntensity getAmbient() const { return ambient; }
	/// <summary>
	/// Specular factor of light
	/// </summary>
	/// <returns>Light specular value</returns>
	float getSpecular() const { return specular; }

	/// <summary>
	/// Shineness factor of light
	/// </summary>
	/// <returns>Light shineness value</returns>
	float getShineness() const { return shineness; }

	/// <summary>
	/// Return pointer to Texture
	/// </summary>
	Texture* getTexture() const { return texture; };

	/// <summary>
	/// Setter of Texture
	/// </summary>
	/// <param name="newTexture">New Texture that will be applied to Material</param>
	void setTexture(Texture* newTexture) { texture = newTexture; }

	/// <summary>
	/// Material Type getter
	/// </summary>
	/// <returns>Type of material</returns>
	MaterialType getMaterialType() { return materialType; }

	/// <summary>
	/// Get material name
	/// </summary>
	/// <returns>Name of material</returns>
	std::string getName() { return name; }

	/// <summary>
	/// Index of refraction getter
	/// </summary>
	/// <returns>Index of Refraction</returns>
	float getRefractionIndex() { return indexOfRefraction; }

private:
	/// <summary>
	/// Ambient part of material
	/// </summary>
	LightIntensity ambient;

	/// <summary>
	/// Diffuse part of material
	/// </summary>
	LightIntensity diffuse;

	/// <summary>
	/// Specular part of material
	/// </summary>
	float specular;

	/// <summary>
	/// Shineness of the material
	/// </summary>
	float shineness;

	/// <summary>
	/// Type of material
	/// </summary>
	MaterialType materialType;

	/// <summary>
	/// Texture assigned to this material
	/// </summary>
	Texture* texture = nullptr;

	/// <summary>
	/// Refraction index
	/// </summary>
	float indexOfRefraction;

	/// <summary>
	/// Name of material
	/// </summary>
	std::string name = "";
};

