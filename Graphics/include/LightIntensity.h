#pragma once

#include "globals.h"
#include <ostream>

class PixelColor;
class Vector3;

/// <summary>
/// Light intensity class. It stores color values (r, g, b) 
/// in floating-point variables which should be between [0, 1].
/// </summary>
class LightIntensity
{
public:
	/// <summary>
	/// Default constructor of LightIntensity. It sets all three colors to 0.
	/// </summary>
	LightIntensity();

	/// <summary>
	/// Constructor of LightIntensity, which sets all three colors to given floating-point values.
	/// </summary>
	/// <param name="r">Value of red component</param>
	/// <param name="g">Value of green component</param>
	/// <param name="b">value of blue component</param>
	LightIntensity(double r, double g, double b);

	/// <summary>
	/// Constructor of light intensity, which takes color of pixel (byte rgb values)
	/// </summary>
	/// <param name="color">Pixel color integeer values in range [0,255]</param>
	LightIntensity(PixelColor color);

	/// <summary>
	/// Vector3 to Light intensity
	/// </summary>
	/// <param name="v">Vector3 object</param>
	LightIntensity(Vector3 v);

	/// <summary>
	/// Constructor of LightIntensity, which sets red and green components to given floating-point values. 
	/// Blue component stays at 0.
	/// </summary>
	/// <param name="r">Value of red component</param>
	/// <param name="g">Value of green component</param>
	LightIntensity(double r, double g);

	/// <summary>
	/// Constructor of LightIntensity, which sets red component to given floating-point value.
	/// Blue and green components stays at 0.
	/// </summary>
	/// <param name="r">Value of red component</param>
	LightIntensity(double r);

	/// <summary>
	/// Clamp LightIntensity values to [0, 1] range
	/// </summary>
	void clampValues();

	/// <summary>
	/// Addition operator for two LightIntensities
	/// </summary>
	/// <param name="addend">Addend of operation</param>
	/// <returns>New LightIntensity object which is sum of this and addend</returns>
	LightIntensity operator+(const LightIntensity& addend) const;

	/// <summary>
	/// Subtraction operator of two LightIntensities
	/// </summary>
	/// <param name="subtrahend">Subtrahend of operation</param>
	/// <returns>New LightIntensity object which is difference of this and subtrahend</returns>
	LightIntensity operator-(const LightIntensity& subtrahend) const;

	/// <summary>
	/// Multiplication operator of LightIntensity and float
	/// </summary>
	/// <param name="factor">Factor of operation</param>
	/// <returns>New LightIntensity object which is product of this and factor</returns>
	LightIntensity operator*(const float& factor) const;
	
	/// <summary>
	/// Multiplication operator of two LightIntensities
	/// </summary>
	/// <param name="factor">Factor of operation</param>
	/// <returns>New LightIntensity object which is product of this and factor</returns>
	LightIntensity operator*(const LightIntensity& factor) const;

	/// <summary>
	/// Division operator of LightIntensity and float
	/// </summary>
	/// <param name="divisor">Divisor of operation</param>
	/// <returns>New LightIntensity object which is quotient of this and divisor</returns>
	LightIntensity operator/(const float& divisor) const;

	/// <summary>
	/// Addition with assignment operator of two LightIntensities
	/// </summary>
	/// <param name="addend">Addend of operation</param>
	/// <returns>Reference to this object after addition the addend to this</returns>
	LightIntensity& operator+=(const LightIntensity& addend);
	/// <summary>
	/// Subtraction with assignment operator of two LightIntensities
	/// </summary>
	/// <param name="subtrahend">Subtrahend of operation</param>
	/// <returns>Reference to this object after subtraction the subtrahend from this</returns>
	LightIntensity& operator-=(const LightIntensity& subtrahend);

	/// <summary>
	/// Multiplication with assignment operator of LightIntensity and float
	/// </summary>
	/// <param name="factor">Factor of operation</param>
	/// <returns>Reference to this object after multiplication of this and factor</returns>
	LightIntensity& operator*=(const float factor);

	/// <summary>
	/// Division with assignment operator of LightIntensity and float
	/// </summary>
	/// <param name="divisor">Divisor of the operation</param>
	/// <returns>Reference to this object after division of this and factor</returns>
	LightIntensity& operator/=(const float divisor);

	/// <summary>
	/// Equality comparison operator with epsilon 0.0001
	/// </summary>
	/// <param name="other">Other Light Intensity to compare</param>
	/// <returns>True if this and other are approximately equal</returns>
	bool operator==(LightIntensity& other) const;

	/// <summary>
	/// Inequality comparison operator with epsilon 0.0001
	/// </summary>
	/// <param name="other">Other Light Intensity to compare</param>
	/// <returns>True if this and other are inequal</returns>
	bool operator!=(LightIntensity& other) const;

	/// <summary>
	/// Getter for red value
	/// </summary>
	/// <returns>Value of red component of LightIntensity</returns>
	double getRed() const { return red; }

	/// <summary>
	/// Getter for green value
	/// </summary>
	/// <returns>Value of green component of LightIntensity</returns>
	double getGreen() const { return green; }

	/// <summary>
	/// Getter for blue value
	/// </summary>
	/// <returns>Value of blue component of LightIntensity</returns>
	double getBlue() const { return blue; }

private:
	/// <summary>
	/// Red component of LightIntensity
	/// </summary>
	double red;
	/// <summary>
	/// Green component of LightIntensity
	/// </summary>
	double green;
	/// <summary>
	/// Blue component of LightIntensity
	/// </summary>
	double blue;
};

/// <summary>
/// Multiplication operator of float and LightIntensity
/// </summary>
/// <param name="factor">Float factor of operation</param>
/// <param name="lFactor">LightIntensity factor of operation</param>
/// <returns>New LightIntensity object which is product of this and factor</returns>
LightIntensity operator*(const float& factor, const LightIntensity& lFactor);

/// <summary>
/// Ostream operator for LightIntensity
/// </summary>
/// <param name="out">Handle to output stream</param>
/// <param name="intensity">LightIntensity object which will be made to string</param>
/// <returns>Handle to output stream with string LightIntensity data</returns>
std::ostream& operator<<(std::ostream& out, LightIntensity& intensity);