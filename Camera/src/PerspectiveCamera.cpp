#include "PerspectiveCamera.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <cstdlib>
#include "PixelColor.h"
#include "Ray.h"
#include "Image.h"
#include "LightIntensity.h"

#include <thread>
#include <intrin.h>
#include <vector>

PerspectiveCamera::PerspectiveCamera(float fov)
{
	position = Vector3::zero;
	target = Vector3::one;
	nearPlane = 0.1f;
	farPlane = 1000.0f;
	this->fov = fov;
	up = Vector3::up;
}

PerspectiveCamera::PerspectiveCamera(Vector3 newPosition, Vector3 newTarget, float fov)
{
	position = newPosition;
	target = newTarget;
	nearPlane = 0.1f;
	farPlane = 1000.0f;
	this->fov = fov;
	up = Vector3::up;
}

float PerspectiveCamera::getFoV()
{
	return fov;
}

LightIntensity PerspectiveCamera::adaptiveAntialiasing(Ray& ray,
	Vector3 A, Vector3 B, Vector3 C, Vector3 D, Vector3 E,
	int depth, const int& maxDepth,
	Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor)
{
	ray.setDestination(E);
	LightIntensity eColor = getColorFromHitable(ray, backgroundColor);

	ray.setDestination(A);
	LightIntensity aColor = getColorFromHitable(ray, backgroundColor);

	ray.setDestination(B);
	LightIntensity bColor = getColorFromHitable(ray, backgroundColor);

	ray.setDestination(C);
	LightIntensity cColor = getColorFromHitable(ray, backgroundColor);

	ray.setDestination(D);
	LightIntensity dColor = getColorFromHitable(ray, backgroundColor);

	/*
	A---B
	|	|
	| E |
	|	|
	D---C
	A, B, C, D, E
	FOR A, E box: A, E + ver, E, E-hor, E-hor/2 + ver/2
	FOR B, E box: E + ver, B, E + hor, E, E + hor/2 + ver/2
	FOR C, E box: E, E+hor, C, E-ver, E + hor/2 - ver/2
	FOR D, E box: E - hor, E, E - ver, D, E-hor/2 - ver/2
	*/

	Vector3 halfHoriz = horizontal * 0.5f;
	Vector3 halfVert = vertical * 0.5f;

	// Check if algorithm reached maximum Depth, if so return color of the middle of subpixel
	if (depth >= maxDepth)
	{
		return eColor;
	}

	if (aColor != eColor)
	{
		Vector3 bPrim = E + vertical;
		Vector3 dPrim = E - horizontal;
		Vector3 ePrim = E - horizontal * 0.5f + vertical * 0.5f;
		aColor = adaptiveAntialiasing(ray, A, bPrim, E, dPrim, ePrim, depth + 1, maxDepth, halfHoriz, halfVert, backgroundColor);
	}
	if (bColor != eColor)
	{
		Vector3 aPrim = E + vertical;
		Vector3 cPrim = E + horizontal;
		Vector3 ePrim = E + horizontal * 0.5f + vertical * 0.5f;
		bColor = adaptiveAntialiasing(ray, aPrim, B, cPrim, E, ePrim, depth + 1, maxDepth, halfHoriz, halfVert, backgroundColor);
	}
	if (cColor != eColor)
	{
		Vector3 bPrim = E + horizontal;
		Vector3 dPrim = E - vertical;
		Vector3 ePrim = E + horizontal * 0.5f - vertical * 0.5f;
		cColor = adaptiveAntialiasing(ray, E, bPrim, C, dPrim, ePrim, depth + 1, maxDepth, halfHoriz, halfVert, backgroundColor);
	}
	if (dColor != eColor)
	{
		Vector3 aPrim = E - horizontal;
		Vector3 cPrim = E - vertical;
		Vector3 ePrim = E - horizontal * 0.5f - vertical * 0.5f;
		dColor = adaptiveAntialiasing(ray, aPrim, E, cPrim, D, ePrim, depth + 1, maxDepth, halfHoriz, halfVert, backgroundColor);
	}

	// get mean of the colors and return them
	return ((aColor + eColor) * 0.5 + (bColor + eColor) * 0.5 + (cColor + eColor) * 0.5 + (dColor + eColor) * 0.5) * 0.25;
}

LightIntensity PerspectiveCamera::regularAntialiasing(Vector3 position, Vector3 leftLowerPixelCorner, const uint n, Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor)
{
	/* 
	Matrix size is NxN, we start from the left lower pixel corner
	N = 5, O is the original target of the ray
	There are 25 rays, 5 in each column/row of the matrix
	Starting from the lower left corner, target of the next ray in the row can be described as such
	Vector3 target = originalTarget + horizontal/5 * iteration; 
	where previousTarget is the Vector3 value of target for previous ray.
	Same can be done with columns, but instead of using horizontal, vertical value is used.
	To get valid result, original target must be moved by horizontal and vertical values divided not by N, but N*2
	(half of the value that is used during iterating).
	Additionally, the value of the color needs to be divided by NxN to get mean value of the color;
	X - - - - - X
	| R R R R R	|
	| R R R R R	|
	| R R R R R	|
	| R R R R R	|
	| O R R R R	|
	X - - - - - X
	*/

	Vector3 horizontalDivided = horizontal / n;
	Vector3 verticalDivided = vertical / n;
	Vector3 lowerLeftCornerTarget = leftLowerPixelCorner + horizontalDivided * 0.5f + verticalDivided * 0.5f;
	float colorDivider = 1.0f / (n * n);

	// PixelColor value that is used as a storage value for values from other rays
	LightIntensity colorValue = LightIntensity(0.0f);

	for (int horizontalIterator = 0; horizontalIterator < n; horizontalIterator++)
	{
		for (int verticalIterator = 0; verticalIterator < n; verticalIterator++)
		{
			Ray ray = Ray(position, Vector3::up);
			ray.setDestination(lowerLeftCornerTarget + horizontalDivided * horizontalIterator + verticalDivided * verticalIterator);
			LightIntensity hitColor = getColorFromHitable(ray, backgroundColor);
			colorValue += hitColor;
		}
	}
	colorValue *= colorDivider;
	return colorValue;
}

LightIntensity PerspectiveCamera::randomAntialiasing(Vector3 position, Vector3 leftLowerPixelCorner, const uint n, Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor)
{
	/*
	Same as regularAntialiasing, but instead of setting target at the middle of the subpixel, randomize int value from 0 to 100, divide it by 100
	and then set destination as lowerLeftCornerTarget + horizontal * horizontalMultiplier + vertical * verticalMultiplier
	*/

	float colorDivider = 1.0f / n;

	// PixelColor value that is used as a storage value for values from other rays
	LightIntensity colorValue = LightIntensity(0.0f);

	for (int i = 0; i < n; i++)
	{
		float horizontalMultiplier = (rand() % 101) * 0.01f;
		float verticalMultiplier = (rand() % 101) * 0.01f;
		Ray ray = Ray(position, Vector3::up);
		ray.setDestination(leftLowerPixelCorner + horizontal * horizontalMultiplier + vertical * verticalMultiplier);
		LightIntensity hitColor = getColorFromHitable(ray, backgroundColor);
		colorValue += hitColor;
	}
	colorValue *= colorDivider;
	return colorValue;
}

LightIntensity PerspectiveCamera::jitterAntialiasing(Vector3 position, Vector3 leftLowerPixelCorner, const uint n, Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor)
{
	/*
	Matrix size is NxN, we start from the left lower pixel corner
	N = 5, O is the original target of the ray
	There are 25 rays, 5 in each column/row of the matrix
	Starting from the lower left corner, target of the next ray in the row can be described as such
	Vector3 target = originalTarget + horizontal/5 * iteration + horizontal/5 * horizontalMultiplier;
	where previousTarget is the Vector3 value of target for previous ray and horizontalMultiplier is value from -0.5f to 0.5f.
	Same can be done with columns, but instead of using horizontal, vertical value is used.
	To get valid result, original target must be moved by horizontal and vertical values divided not by N, but N*2
	(half of the value that is used during iterating).
	Additionally, the value of the color needs to be divided by NxN to get mean value of the color;
	X - - - - - X
	| R R R R R	|
	| R R R R R	|
	| R R R R R	|
	| R R R R R	|
	| O R R R R	|
	X - - - - - X
	*/

	Vector3 horizontalDivided = horizontal / n;
	Vector3 verticalDivided = vertical / n;
	Vector3 lowerLeftCornerTarget = leftLowerPixelCorner + horizontalDivided * 0.5f + verticalDivided * 0.5f;
	float colorDivider = 1.0f / (n * n);

	// PixelColor value that is used as a storage value for values from other rays
	LightIntensity colorValue = LightIntensity(0.0f);

	for (int horizontalIterator = 0; horizontalIterator < n; horizontalIterator++)
	{
		for (int verticalIterator = 0; verticalIterator < n; verticalIterator++)
		{
			float horizontalMultiplier = ((rand() % 101) - 50) * 0.01f;
			float verticalMultiplier = ((rand() % 101) - 50)* 0.01f;
			Ray ray = Ray(position, Vector3::up);
			ray.setDestination(lowerLeftCornerTarget + horizontalDivided * horizontalIterator + horizontalDivided * horizontalMultiplier + verticalDivided * verticalIterator + verticalDivided * verticalMultiplier);
			LightIntensity hitColor = getColorFromHitable(ray, backgroundColor);
			colorValue += hitColor;
		}
	}
	colorValue *= colorDivider;
	return colorValue;
}

LightIntensity PerspectiveCamera::poissonDiskAntialiasing(Vector3 position, Vector3 leftLowerPixelCorner, const uint n, const float diff, Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor)
{
	/*
	Same as randomAntialiasing, but with one key difference - each target point is put into vector and then is compared
	in next iterations. If two points are too close to each other, mark new target point.
	Repeat until there is no difference;
	*/

	float colorDivider = 1.0f / n;
	float pointDifference = horizontal.getLength() * POISSON_DISK_DIFFERENCE;

	// PixelColor value that is used as a storage value for values from other rays
	LightIntensity colorValue = LightIntensity(0.0f);

	std::vector<Vector3> points;

	for (int i = 0; i < n; i++)
	{
		bool passedCheck = false;
		Vector3 target = Vector3::zero;
		while (!passedCheck)
		{
			passedCheck = true;
			float horizontalMultiplier = (rand() % 101) * 0.01f;
			float verticalMultiplier = (rand() % 101) * 0.01f;
			target = leftLowerPixelCorner + horizontal * horizontalMultiplier + vertical * verticalMultiplier;
			for (Vector3 point : points)
			{
				Vector3 diffPoint = point - target;
				float diffPointLen = diffPoint.getLength() * 0.01f;
				if (diffPointLen < pointDifference)
				{
					passedCheck = false;
					break;
				}
			}
		}
		points.push_back(target);
		Ray ray = Ray(position, Vector3::up);
		ray.setDestination(target);
		LightIntensity hitColor = getColorFromHitable(ray, backgroundColor);
		colorValue += hitColor;
	}
	colorValue *= colorDivider;
	return colorValue;
}

Vector3 PerspectiveCamera::getUp()
{
	return up;
}

void PerspectiveCamera::renderScene(Image* bufferImage, Antyaliasing antyaliasingType)
{
	// Coordinates viewPlane 
	Vector3 w, u, v;

	float theta = fov * M_PI / 180.0f;
	float halfHeight = tanf(theta / 2.0f);
	float aspect = bufferImage->getWidth() / bufferImage->getHeight();
	float halfWidth = aspect * halfHeight;

	// nearPlane is used to mark viewFrustrum
	//nearPlane = (target - position).getLength();

	// W, U, V coordinates, V and U are swapped places and v is negated compared to original
	w = (position - target).normalized();
	u = -(up.cross(w)).normalized();
	v = w.cross(u).normalized();

	float u0 = position.getX() - halfWidth * nearPlane;
	float v0 = position.getY() - halfHeight * nearPlane;

	// lower left corner of ViewPlane
	lowerLeftCorner = u * u0 + v * v0 - nearPlane * w;

	// lower right corner of ViewPlane
	horizontal = (2 * halfWidth * nearPlane) * u;

	// upper left corner of ViewPlane
	vertical = (2 * halfHeight * nearPlane) * v;

	// size of singular pixel in viewPlane
	pixelHorizontal = horizontal / bufferImage->getWidth();
	pixelVertical = vertical / bufferImage->getHeight();

	std::vector<std::thread> threads;
	unsigned threadLimit = std::thread::hardware_concurrency();
	volatile unsigned threadsCount = 0;

	for (int i = 0; i < bufferImage->getWidth(); i++)
	{
		while (threadsCount >= threadLimit) { __nop(); }
		threadsCount++;
		threads.push_back(std::thread([this, i, bufferImage, antyaliasingType](volatile unsigned* threadsCounter)
		{
			for (int j = 0; j < bufferImage->getHeight(); j++)
			{
				
				// Get background color from buffer, used in antialiasing
				PixelColor backgroundColor = bufferImage->getPixelColor(i, j);
				PixelColor finalColor = PixelColor(0);
				Vector3 aliasingVariable = lowerLeftCorner + pixelVertical * j + pixelHorizontal * i;
				// antialiasing
				switch (antyaliasingType)
				{
				case Antyaliasing::REGULAR:
					finalColor = regularAntialiasing(position, aliasingVariable, MATRIX_SIZE, pixelHorizontal, pixelVertical, backgroundColor);
					break;
				case Antyaliasing::RANDOM:
					finalColor = randomAntialiasing(position, aliasingVariable, RANDOM_SAMPLE_SIZE, pixelHorizontal, pixelVertical, backgroundColor);
					break;
				case Antyaliasing::JITTER:
					finalColor = jitterAntialiasing(position, aliasingVariable, MATRIX_SIZE, pixelHorizontal, pixelVertical, backgroundColor);
					break;
				case Antyaliasing::DISK:
					finalColor = poissonDiskAntialiasing(position, aliasingVariable, RANDOM_SAMPLE_SIZE, POISSON_DISK_DIFFERENCE, pixelHorizontal, pixelVertical, backgroundColor);
					break;
				case Antyaliasing::ADAPTIVE:
					finalColor = adaptiveSetup(lowerLeftCorner, pixelVertical, pixelHorizontal, i, j, backgroundColor);
					break;
				case Antyaliasing::NONE:
				{
					Ray r = Ray(position, Vector3::up);
					r.setDestination(aliasingVariable);
					finalColor = getColorFromHitable(r, backgroundColor);
				}
				}

				// set pixel color 
				bufferImage->setPixelColor(i, j, finalColor);
			}
			(*threadsCounter)--;
		}, &threadsCount));
	}

	for (auto& th : threads)
	{
		th.join();
	}
}

LightIntensity PerspectiveCamera::adaptiveSetup(Vector3 lowerLeft, Vector3 pixelVertical, Vector3 pixelHorizontal, int x, int y, const PixelColor backgroundColor)
{
	//// New Ray, direction will be changed in antialiasing
	Ray ray = Ray(position, Vector3(0.0f, 0.0f, 1.0f));

	// Calculate middle of pixel and  corners of the pixel, A, B, C, D, look at antialiasing for exact positions
	Vector3 rayTarget = lowerLeftCorner + pixelVertical * y + pixelHorizontal * x + pixelHorizontal / 2 + pixelVertical / 2;
	Vector3 aCorner = rayTarget - pixelHorizontal / 2 + pixelVertical / 2;
	Vector3 bCorner = rayTarget + pixelHorizontal / 2 + pixelVertical / 2;
	Vector3 cCorner = rayTarget + pixelHorizontal / 2 - pixelVertical / 2;
	Vector3 dCorner = rayTarget - pixelHorizontal / 2 - pixelVertical / 2;
	return adaptiveAntialiasing(ray, aCorner, bCorner, cCorner, dCorner, rayTarget,
		0, MAX_RECURSIVE_ANTYALIASING, pixelHorizontal / 2, pixelVertical / 2, backgroundColor);
}