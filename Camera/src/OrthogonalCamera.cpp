#include "OrthogonalCamera.h"
#include "PixelColor.h"
#include "Image.h"
#include "Ray.h"
#include "LightIntensity.h"

void OrthogonalCamera::renderScene(Image* bufferImage, Antyaliasing antyaliasingType)
{
	// rozmiar piksela w przestrzeni
	float pixelWidth = 2.0f / bufferImage->getHeight();
	float pixelHeight = 2.0f / bufferImage->getWidth();

	float middleX, middleY;

	for (int i = 0; i < bufferImage->getWidth(); i++)
	{
		for (int j = 0; j < bufferImage->getHeight(); j++)
		{
			// size of the pixel in space
			middleX = -1.0f + (i + 0.5f) * pixelWidth;
			middleY = 1.0f - (j + 0.5f) * pixelHeight;

			// ray starts at the middle of pixel but at 0, with direction at the middle of pixel
			Ray ray = Ray(Vector3(middleX, middleY, 0.0f), Vector3(0.0f, 0.0f, 1.0f));

			// Calculate middle of pixel and  corners of the pixel, A, B, C, D, look at antialiasing for exact positions
			Vector3 rayPosition = ray.getOrigin();
			Vector3 aCorner = Vector3(middleX - pixelWidth / 2.0f, middleY + pixelHeight / 2.0f, 0.0f);
			Vector3 bCorner = Vector3(middleX + pixelWidth / 2.0f, middleY + pixelHeight / 2.0f, 0.0f);
			Vector3 cCorner = Vector3(middleX + pixelWidth / 2.0f, middleY - pixelHeight / 2.0f, 0.0f);
			Vector3 dCorner = Vector3(middleX - pixelWidth / 2.0f, middleY - pixelHeight / 2.0f, 0.0f);

			// Offset used in antialiasing
			Vector3 horizontal = Vector3(pixelWidth / 2.0f, 0.0f, 0.0f);
			Vector3 vertical = Vector3(0.0f, pixelHeight / 2.0f, 0.0f);

			// Get background color from buffer, used in antialiasing
			PixelColor backgroundColor = bufferImage->getPixelColor(i, j);

			// antialiasing
			PixelColor finalColor = adaptiveAntialiasing(ray, aCorner, bCorner, cCorner, dCorner, rayPosition,
				0, 1, horizontal, vertical, backgroundColor);

			// set pixel color 
			bufferImage->setPixelColor(i, j, finalColor);
		}
	}
}

LightIntensity OrthogonalCamera::adaptiveAntialiasing(Ray& ray,
	Vector3 A, Vector3 B, Vector3 C, Vector3 D, Vector3 E,
	int depth, const int& maxDepth,
	Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor)
{
	/*
	A---B
	|	|
	| E |
	|	|
	D---C
	A, B, C, D, E
	FOR A, E box: A, E + up, E, E-hor, E-hor/2 + ver/2
	FOR B, E box: E+ver, B, E + hor, E, E + hor/2 + ver/2
	FOR C, E box: E, E+hor, C, E-ver, E + hor/2 - ver/2
	FOR D, E box: E - hor, E, E - ver, D, E-hor/2 - ver/2
	*/
	ray.setOrigin(E);
	LightIntensity eColor = getColorFromHitable(ray, backgroundColor);

	ray.setOrigin(A);
	LightIntensity aColor = getColorFromHitable(ray, backgroundColor);

	ray.setOrigin(B);
	LightIntensity bColor = getColorFromHitable(ray, backgroundColor);

	ray.setOrigin(C);
	LightIntensity cColor = getColorFromHitable(ray, backgroundColor);

	ray.setOrigin(D);
	LightIntensity dColor = getColorFromHitable(ray, backgroundColor);

	// Check if algorithm reached maximum Depth, if so return color of the middle of subpixel
	if (depth >= maxDepth)
	{
		return eColor;
	}

	// Check if colors are different at corners, if so create subpixel and go deeper
	if (aColor != eColor)
	{
		Vector3 bPrim = E + vertical;
		Vector3 dPrim = E - horizontal;
		Vector3 ePrim = E - horizontal / 2 + vertical / 2;
		aColor = adaptiveAntialiasing(ray, A, bPrim, E, dPrim, ePrim, depth + 1, maxDepth, horizontal / 2, vertical / 2, backgroundColor);
	}
	if (bColor != eColor)
	{
		Vector3 aPrim = E + vertical;
		Vector3 cPrim = E + horizontal;
		Vector3 ePrim = E + horizontal / 2 + vertical / 2;
		bColor = adaptiveAntialiasing(ray, aPrim, B, cPrim, E, ePrim, depth + 1, maxDepth, horizontal / 2, vertical / 2, backgroundColor);
	}
	if (cColor != eColor)
	{
		Vector3 bPrim = E + horizontal;
		Vector3 dPrim = E - vertical;
		Vector3 ePrim = E + horizontal / 2 - vertical / 2;
		cColor = adaptiveAntialiasing(ray, E, bPrim, C, dPrim, ePrim, depth + 1, maxDepth, horizontal / 2, vertical / 2, backgroundColor);
	}
	if (dColor != eColor)
	{
		Vector3 aPrim = E - horizontal;
		Vector3 cPrim = E - vertical;
		Vector3 ePrim = E - horizontal / 2 - vertical / 2;
		dColor = adaptiveAntialiasing(ray, aPrim, E, cPrim, D, ePrim, depth + 1, maxDepth, horizontal / 2, vertical / 2, backgroundColor);
	}

	// get mean of the colors and return them
	return ((aColor + eColor) * 0.5 + (bColor + eColor) * 0.5 + (cColor + eColor) * 0.5 + (dColor + eColor) * 0.5) * 0.25;
}
