#include "Camera.h"
#include "Scene.h"
#include "SceneObject.h"
#include "Material.h"
#include "Hitable.h"
#include "PixelColor.h"
#include "Ray.h"
#include "Light.h"
#include "PointLight.h"
#include "DirectionalLight.h"
#include "Vector2.h"
#include "Material.h"
#include "Texture.h"
#include "Mesh.h"

Camera::Camera()
{
	position = Vector3::zero;
	target = Vector3::one;
	nearPlane = 0.1f;
	farPlane = 20.0f;
}

Camera::Camera(Vector3 newPosition, Vector3 newTarget)
{
	position = newPosition;
	target = newTarget;
	nearPlane = 0.1f;
	farPlane = 20.0f;
}

Vector3 Camera::getPostition()
{
	return position;
}

Vector3 Camera::getTarget()
{
	return target;
}

float Camera::getNearPlane()
{
	return nearPlane;
}

float Camera::getFarPlane()
{
	return farPlane;
}

LightIntensity Camera::getColorFromHitable(Ray ray, PixelColor backgroundColor)
{
	auto sceneVector = scene->getScene();
	float dist = farPlane;
	LightIntensity returnColor = backgroundColor;
	SceneObject* closestObject = analyzeScene(ray, nullptr, dist);

	if (closestObject != nullptr)
	{
		if (scene->getLights()->size() == 0 && scene->getDirectional() == nullptr) //No lights
		{
			Material* mat = closestObject->getMaterial();
			//Material diffuse
			returnColor = closestObject->getObjectDiffuse(ray.pointAtDistance(dist));
		}
		else
		{
			//returnColor = closestObject->getGeometry()->normalInPoint(ray.pointAtDistance(dist));
			returnColor = recursiveRaytracing(ray.pointAtDistance(dist), closestObject, MAX_RECURSIVE_LEVEL);
			returnColor.clampValues();
		}
	}
	return returnColor;
}

LightIntensity Camera::recursiveRaytracing(Vector3 intersectionPoint, SceneObject* closestObject, int recurseLevel)
{
	LightIntensity returnColor(0);
	Vector3 viewDir = (target - position).normalized();
	float maxDistance = INFINITY;
	if (closestObject->getMaterial()->getMaterialType() == MaterialType::NONE)
	{
		returnColor += closestObject->getMaterial()->getDiffuse();
	}
	else if (closestObject->getMaterial()->getMaterialType() == MaterialType::NORMAL || recurseLevel <= 0)
	{
		auto* dir = scene->getDirectional();
		if (dir != nullptr)
		{
			Ray shadowRay = Ray(intersectionPoint, -dir->getLightDirection(intersectionPoint));
			auto shadedObject = analyzeScene(shadowRay, closestObject, maxDistance, false, true);
			returnColor += dir->calculateLight(closestObject, intersectionPoint, viewDir, shadedObject != nullptr);
		}
		for (Light* l : *scene->getLights())
		{
			maxDistance = (intersectionPoint - l->getLightPosition()).getLength();
			Ray shadowRay = Ray(intersectionPoint, -l->getLightDirection(intersectionPoint));
			auto shadedObject = analyzeScene(shadowRay, closestObject, maxDistance, false, true);
			returnColor += l->calculateLight(closestObject, intersectionPoint, viewDir, shadedObject != nullptr);
		}
		returnColor += closestObject->getMaterial()->getAmbient();
	}
	else if (recurseLevel > 0)
	{
		Vector3 fromCamera = (intersectionPoint - position).normalized();
		Vector3 nextDir = Vector3::zero;
		Ray nextRay = Ray();
		Vector3 normal = closestObject->getGeometry()->normalInPoint(intersectionPoint);
		SceneObject* shadedObject = nullptr;
		if (closestObject->getMaterial()->getMaterialType() == MaterialType::MIRROR)
		{
			nextDir = fromCamera - 2 * normal * normal.dot(fromCamera);
		}
		else if (closestObject->getMaterial()->getMaterialType() == MaterialType::GLASS)
		{
			float dnDot = fromCamera.dot(normal);
			
			float refractIndex = closestObject->getMaterial()->getRefractionIndex();
			nextDir = (fromCamera - normal * dnDot) / refractIndex - normal * sqrt(1 - (1 - dnDot * dnDot) / (refractIndex * refractIndex));
			nextRay = Ray(intersectionPoint - (normal * 0.1f), nextDir);
			// shade itself and went out with ray
			maxDistance = farPlane;
			shadedObject = analyzeScene(nextRay, closestObject, maxDistance, true);
			if (shadedObject == closestObject)
			{
				intersectionPoint = nextRay.pointAtDistance(maxDistance);
				normal = closestObject->getGeometry()->normalInPoint(intersectionPoint);
				float dnDot = nextDir.dot(normal);
				nextDir = refractIndex * (nextDir - normal * dnDot) - normal * sqrt(1 - (refractIndex * refractIndex) * (1 - dnDot * dnDot));
			}
		}
		nextRay = Ray(intersectionPoint, nextDir);
		maxDistance = farPlane;
		shadedObject = analyzeScene(nextRay, closestObject, maxDistance);
		
		if (shadedObject != nullptr)
		{
			returnColor += recursiveRaytracing(nextRay.pointAtDistance(maxDistance), shadedObject, recurseLevel - 1);
		}
	}
	return returnColor;
}


void Camera::assignScene(Scene* scene)
{
	this->scene = scene;
}

SceneObject* Camera::analyzeScene(Ray& ray, SceneObject* closestObject, float& maxDistance, bool selfIntersect, bool light)
{
	float newDistance = maxDistance + 1;
	SceneObject* closest = nullptr;
	for (SceneObject* sceneObject : *scene->getScene())
	{
		if (sceneObject->getMaterial()->getMaterialType() == MaterialType::NONE && light)
		{
			continue;
		}
		if (sceneObject == closestObject && !selfIntersect)
		{
			continue;
		}
		newDistance = sceneObject->getGeometry()->countIntersection(ray, maxDistance, true);
		if (newDistance < maxDistance)
		{
			maxDistance = newDistance;
			closest = sceneObject;
		}
	}

	for (auto* m : *scene->getSceneMeshes())
	{
		if (m->testOBB(ray, maxDistance))
		{
			for (auto* sceneObject : *m->getTriangles())
			{
				if (sceneObject == closestObject && !selfIntersect)
				{
					continue;
				}
				newDistance = sceneObject->getGeometry()->countIntersection(ray, maxDistance, true);
				if (newDistance < maxDistance)
				{
					maxDistance = newDistance;
					closest = sceneObject;
				}
			}
		}
	}
	return closest;
}
