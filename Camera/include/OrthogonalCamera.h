#include "Camera.h"

/// <summary>
/// Orthogonal Camera class
/// </summary>
class OrthogonalCamera : public Camera
{
public:

	/// <summary>
	/// Constructor for class OrthogonalCamera with extended parameters
	/// </summary>
	/// <param name="newPosition">Vector3 that represents position of OrthogonalCamera in space</param>
	/// <param name="newTarget">Vector3 that represents target of OrthogonalCamera in space</param>
	OrthogonalCamera(Vector3 newPosition, Vector3 newTarget) : Camera(newPosition, newTarget) {}

	/// <summary>
	/// Render Scene and saves data in Image object class
	/// </summary>
	/// <param name="bufferImage">Pointer to Image object where image is stored for saving</param>
	/// <param name="antyaliasingType">Type of antyaliasing</param>
	void renderScene(Image* bufferImage, Antyaliasing antyaliasingType);

private:
	/// <summary>
	/// Implementation of recursive Adaptive Antialiasing in Orthogonal Camera
	/// </summary>
	/// <param name="ray">Ray that will be used to check interaction with hitable objects</param>
	/// <param name="A">Position of left upper corner of pixel represented in Vector3</param>
	/// <param name="B">Position of right upper corner of pixel represented in Vector3</param>
	/// <param name="C">Position of right down corner of pixel represented in Vector3</param>
	/// <param name="D">Position of left down corner of pixel represented in Vector3</param>
	/// <param name="E">Position of the center of pixel represented in Vector</param>
	/// <param name="depth">Current depth of the algorithm</param>
	/// <param name="maxDepth">Max depth of the algorithm, if its reached, return color from the center of the pixel</param>
	/// <param name="horizontal">Vector3 value that represents horizontal offset from the center of pixel to horizontal edge</param>
	/// <param name="vertical">Vector3 value that represents vertical offset from the center of pixel to vertical edge</param>
	/// <param name="backgroundColor">Color of the background used if Ray does not hit any Hitable objects</param>
	/// <returns>Color value in pixel after applying Adaptive Antialiasing</returns>
	LightIntensity adaptiveAntialiasing(Ray& ray,
		Vector3 A, Vector3 B, Vector3 C, Vector3 D, Vector3 E,
		int depth, const int& maxDepth,
		Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor);
};