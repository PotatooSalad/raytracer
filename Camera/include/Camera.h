#pragma once

#include "Vector3.h"
#include "globals.h"

class Scene;
class SceneObject;
class Material;
class PixelColor;
class LightIntensity;
class Image;
class Ray;

/// <summary>
/// Antyaliasing type enum
/// </summary>
enum class Antyaliasing : uint
{
	REGULAR,
	RANDOM,
	JITTER,
	DISK,
	ADAPTIVE,
	NONE
};

/// <summary>
/// Class that defines Camera that is later used in derived classes
/// </summary>
class Camera
{
public:
	/// <summary>
	/// Default constructor for class Camera
	/// </summary>
	Camera();

	/// <summary>
	/// Constructor for class Camera with extended parameters
	/// </summary>
	/// <param name="newPosition">Vector3 that represents position of Camera in space</param>
	/// <param name="newTarget">Vector3 that represents target of Camera in space</param>
	Camera(Vector3 newPosition, Vector3 newTarget);

	/// <summary>
	/// Getter of position
	/// </summary>
	/// <returns>Vector3 value of position</returns>
	Vector3 getPostition();

	/// <summary>
	/// Getter of target
	/// </summary>
	/// <returns>Vector3 value of target</returns>
	Vector3 getTarget();

	/// <summary>
	/// Getter of nearPlane
	/// </summary>
	/// <returns>Float value of nearPlane</returns>
	float getNearPlane();

	/// <summary>
	/// Getter of farPlane
	/// </summary>
	/// <returns>Float value of farPlane</returns>
	float getFarPlane();

	/// <summary>
	/// Virtual method of rendering scene for override in Derived classes
	/// </summary>
	/// <param name="bufferImage">Pointer to Image object where image is stored for saving</param>
	/// <param name="antyaliasingType">Type of antyaliasing</param>
	virtual void renderScene(Image* bufferImage, Antyaliasing antyaliasingType) = 0;

	/// <summary>
	/// Get Color from Hitable object that Ray hit it
	/// </summary>
	/// <param name="ray">Ray that is used to check if it hit any Hitable in Scene</param>
	/// <param name="backgroundColor">Color that is returned if no Hitable was hit</param>
	/// <returns>Color value of either Hitable object in that point or background Color</returns>
	LightIntensity getColorFromHitable(Ray ray, PixelColor backgroundColor);

	/// <summary>
	/// Assigns Scene to 
	/// </summary>
	/// <param name="scene">Pointer to Scene that is then assigned to Camera</param>
	void assignScene(Scene* scene);


protected:
	/// <summary>
	/// Matrix size for regular and jitter antyaliasing
	/// </summary>
	const int MATRIX_SIZE = 5;
	/// <summary>
	/// Size of random sample for poisson and random antyaliasing
	/// </summary>
	const int RANDOM_SAMPLE_SIZE = 25;
	/// <summary>
	/// Poisson disk difference
	/// </summary>
	const float POISSON_DISK_DIFFERENCE = 0.001725f; // percentage of pixel, 0.001f works fine and 0.001725f works too; 0.00175f is too much tho
	/// <summary>
	/// Max recursive expansion for recursive antyaliasing
	/// </summary>
	const int MAX_RECURSIVE_ANTYALIASING = 1;

	/// <summary>
	/// Implementation of recursive Adaptive Antialiasing
	/// </summary>
	/// <param name="ray">Ray that will be used to check interaction with hitable objects</param>
	/// <param name="A">Position of left upper corner of pixel represented in Vector3</param>
	/// <param name="B">Position of right upper corner of pixel represented in Vector3</param>
	/// <param name="C">Position of right down corner of pixel represented in Vector3</param>
	/// <param name="D">Position of left down corner of pixel represented in Vector3</param>
	/// <param name="E">Position of the center of pixel represented in Vector</param>
	/// <param name="depth">Current depth of the algorithm</param>
	/// <param name="maxDepth">Max depth of the algorithm, if its reached, return color from the center of the pixel</param>
	/// <param name="horizontal">Vector3 value that represents horizontal offset from the center of pixel to horizontal edge</param>
	/// <param name="vertical">Vector3 value that represents vertical offset from the center of pixel to vertical edge</param>
	/// <param name="backgroundColor">Color of the background used if Ray does not hit any Hitable objects</param>
	/// <returns>Color value in pixel after applying Adaptive Antialiasing</returns>
	virtual LightIntensity adaptiveAntialiasing(Ray& ray,
		Vector3 A, Vector3 B, Vector3 C, Vector3 D, Vector3 E,
		int depth, const int& maxDepth,
		Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor) = 0;

	/// <summary>
	/// Implementation of Regular Antialiasing
	/// </summary>
	/// <param name="position">Vector3 that represents position of the Camera</param>
	/// <param name="leftLowerPixelCorner">Vector3 value that represents left lower corner of Pixel</param>
	/// <param name="n">Size of matrix that represents pixel according to equasion n x n</param>
	/// <param name="horizontal">Vector3 value that represents horizontal offset from the left lower corner of pixel to right lower corner of pixel</param>
	/// <param name="vertical">Vector3 value that represents vertical offset from the left lower corner of pixel to left upper corner of pixel</param>
	/// <param name="backgroundColor">Color of the background used if Ray does not hit any Hitable objects</param>
	/// <returns>Color value in pixel after applying Regular Antialiasing</returns>
	virtual LightIntensity regularAntialiasing(Vector3 position, Vector3 leftLowerPixelCorner, const uint n, Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor) = 0;

	/// <summary>
	/// Implementation of Random Antialiasing
	/// </summary>
	/// <param name="position">Vector3 that represents position of the Camera</param>
	/// <param name="leftLowerPixelCorner">Vector3 value that represents left lower corner of Pixel</param>
	/// <param name="n">Size of samples taken</param>
	/// <param name="horizontal">Vector3 value that represents horizontal offset from the left lower corner of pixel to right lower corner of pixel</param>
	/// <param name="vertical">Vector3 value that represents vertical offset from the left lower corner of pixel to left upper corner of pixel</param>
	/// <param name="backgroundColor">Color of the background used if Ray does not hit any Hitable objects</param>
	/// <returns>Color value in pixel after applying Random Antialiasing</returns>
	virtual LightIntensity randomAntialiasing(Vector3 position, Vector3 leftLowerPixelCorner, const uint n, Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor) = 0;

	/// <summary>
	/// Implementation of Jitter Antialiasing
	/// </summary>
	/// <param name="position">Vector3 that represents position of the Camera</param>
	/// <param name="leftLowerPixelCorner">Vector3 value that represents left lower corner of Pixel</param>
	/// <param name="n">Size of matrix that represents pixel according to equasion n x n</param>
	/// <param name="horizontal">Vector3 value that represents horizontal offset from the left lower corner of pixel to right lower corner of pixel</param>
	/// <param name="vertical">Vector3 value that represents vertical offset from the left lower corner of pixel to left upper corner of pixel</param>
	/// <param name="backgroundColor">Color of the background used if Ray does not hit any Hitable objects</param>
	/// <returns>Color value in pixel after applying Jitter Antialiasing</returns>
	virtual LightIntensity jitterAntialiasing(Vector3 position, Vector3 leftLowerPixelCorner, const uint n, Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor) = 0;

	/// <summary>
	/// Implementation of Poisson Disk Antialiasing
	/// </summary>
	/// <param name="position">Vector3 that represents position of the Camera</param>
	/// <param name="leftLowerPixelCorner">Vector3 value that represents left lower corner of Pixel</param>
	/// <param name="n">Size of samples taken</param>
	/// <param name="diff">Value of the difference used in determining if target should be marked again</param>
	/// <param name="horizontal">Vector3 value that represents horizontal offset from the left lower corner of pixel to right lower corner of pixel</param>
	/// <param name="vertical">Vector3 value that represents vertical offset from the left lower corner of pixel to left upper corner of pixel</param>
	/// <param name="backgroundColor">Color of the background used if Ray does not hit any Hitable objects</param>
	/// <returns>Color value in pixel after applying Poisson Disk Antialiasing</returns>
	virtual LightIntensity poissonDiskAntialiasing(Vector3 position, Vector3 leftLowerPixelCorner, const uint n, const float diff, Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor) = 0;

	/// <summary>
	/// Near Plane represented by float
	/// </summary>
	float nearPlane;

	/// <summary>
	/// Far Plane represented by float
	/// </summary>
	float farPlane;

	/// <summary>
	/// Position of Camera represented by Vector3
	/// </summary>
	Vector3 position;

	/// <summary>
	/// Target of Camera represented by Vector3
	/// </summary>
	Vector3 target;

private:
	/// <summary>
	/// Pointer to Scene object
	/// </summary>
	Scene* scene = nullptr;

	/// <summary>
	/// Maximum recurse level for recursive raytracing
	/// </summary>
	const int MAX_RECURSIVE_LEVEL = 2;

	/// <summary>
	/// Analyze scene to get closest object from ray
	/// </summary>
	/// <param name="ray">Ray objec(base to search intersections)</param>
	/// <param name="closestObject">Closest object (for now)</param>
	/// <param name="maxDistance">Maximum distance to search</param>
	/// <param name="selfIntersect">Should object self-intersect</param>
	/// <param name="light">Is this analyzing for light? (omitting NONE material type)</param>
	/// <returns>Pointer to closest scene object or nullptr if that object doesn't exist</returns>
	SceneObject* analyzeScene(Ray& ray, SceneObject* closestObject, float& maxDistance, bool selfIntersect = false, bool light = false);

	/// <summary>
	/// Recursive raytracing method
	/// </summary>
	/// <param name="intersectionPoint">Last intersection point</param>
	/// <param name="closestObject">Object losest to last ray</param>
	/// <param name="recurseLevel">Level of recursion (from MAX to 0)</param>
	/// <returns></returns>
	LightIntensity recursiveRaytracing(Vector3 intersectionPoint, SceneObject* closestObject, int recurseLevel);
};