#pragma once
#include "Camera.h"

/// <summary>
/// Perspective Camera class
/// </summary>
class PerspectiveCamera : public Camera
{
public:
	/// <summary>
	/// Default constructor for class PerspectiveCamera
	/// </summary>
	/// <param name="fov">Camera's field of view</param>
	PerspectiveCamera(float fov);

	/// <summary>
	/// Constructor for class PerspectiveCamera with extended parameters
	/// </summary>
	/// <param name="newPosition">Vector3 that represents position of PerspectiveCamera in space</param>
	/// <param name="newTarget">Vector3 that represents target of PerspectiveCamera in space</param>
	/// <param name="fov">Camera's field of view</param>
	PerspectiveCamera(Vector3 newPosition, Vector3 newTarget, float fov);

	/// <summary>
	/// Render Scene and saves data in Image object class
	/// </summary>
	/// <param name="bufferImage">Pointer to Image object where image is stored for saving</param>
	/// <param name="antyaliasingType">Type of antyaliasing</param>
	void renderScene(Image* bufferImage, Antyaliasing antyaliasingType);

	/// <summary>
	/// Getter of Field of View
	/// </summary>
	/// <returns>Float value of Field of View</returns>
	float getFoV();

	/// <summary>
	/// Setter of Field of View
	/// </summary>
	/// <param name="fov">New value of camera's field of view</param>
	void setFoV(float fov) { this->fov = fov; }

	/// <summary>
	/// Getter of Up value
	/// </summary>
	/// <returns>Vector3 value of Up</returns>
	Vector3 getUp();

private:
	/// <summary>
	/// Field of View of Camera represented by float
	/// </summary>
	float fov;

	/// <summary>
	/// Vector3 that represents UP vector "attached" at the top of the Camera
	/// </summary>
	Vector3 up = Vector3::up;

	/// <summary>
	/// Implementation of Regular Antialiasing in Perspective Camera
	/// </summary>
	/// <param name="ray">Ray that will be used to check interaction with hitable objects</param>
	/// <param name="A">Position of left upper corner of pixel represented in Vector3</param>
	/// <param name="B">Position of right upper corner of pixel represented in Vector3</param>
	/// <param name="C">Position of right down corner of pixel represented in Vector3</param>
	/// <param name="D">Position of left down corner of pixel represented in Vector3</param>
	/// <param name="E">Position of the center of pixel represented in Vector</param>
	/// <param name="depth">Current depth of the algorithm</param>
	/// <param name="maxDepth">Max depth of the algorithm, if its reached, return color from the center of the pixel</param>
	/// <param name="horizontal">Vector3 value that represents horizontal offset from the center of pixel to horizontal edge</param>
	/// <param name="vertical">Vector3 value that represents vertical offset from the center of pixel to vertical edge</param>
	/// <param name="backgroundColor">Color of the background used if Ray does not hit any Hitable objects</param>
	/// <returns>Color value in pixel after applying Adaptive Antialiasing</returns>
	LightIntensity adaptiveAntialiasing(Ray& ray,
		Vector3 A, Vector3 B, Vector3 C, Vector3 D, Vector3 E,
		int depth, const int& maxDepth,
		Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor);

	/// <summary>
	/// Adaptive antyaliasing setup and run method
	/// </summary>
	/// <param name="lowerLeft">Lower left corner of image</param>
	/// <param name="pixelVertical">Size of vertical pixel</param>
	/// <param name="pixelHorizontal">Size of horizontal pixel</param>
	/// <param name="x">Pixel horizontal index</param>
	/// <param name="y">Pixel vertical index</param>
	/// <param name="backgroundColor">Background color of image in x y</param>
	/// <returns>Color value in pixel after applying Adaptive Antialiasing</returns>
	LightIntensity adaptiveSetup(Vector3 lowerLeft, Vector3 pixelVertical, Vector3 pixelHorizontal, int x, int y, const PixelColor backgroundColor);

	/// <summary>
	/// Implementation of Regular Antialiasing in Perspective Camera
	/// </summary>
	/// <param name="position">Vector3 that represents position of the Camera</param>
	/// <param name="leftLowerPixelCorner">Vector3 value that represents left lower corner of Pixel</param>
	/// <param name="n">Size of matrix that represents pixel according to equasion n x n</param>
	/// <param name="horizontal">Vector3 value that represents horizontal offset from the left lower corner of pixel to right lower corner of pixel</param>
	/// <param name="vertical">Vector3 value that represents vertical offset from the left lower corner of pixel to left upper corner of pixel</param>
	/// <param name="backgroundColor">Color of the background used if Ray does not hit any Hitable objects</param>
	/// <returns>Color value in pixel after applying Regular Antialiasing</returns>
	LightIntensity regularAntialiasing(Vector3 position, Vector3 leftLowerPixelCorner, const uint n, Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor);

	/// <summary>
	/// Implementation of Random Antialiasing
	/// </summary>
	/// <param name="position">Vector3 that represents position of the Camera</param>
	/// <param name="leftLowerPixelCorner">Vector3 value that represents left lower corner of Pixel</param>
	/// <param name="n">Size of samples taken</param>
	/// <param name="horizontal">Vector3 value that represents horizontal offset from the left lower corner of pixel to right lower corner of pixel</param>
	/// <param name="vertical">Vector3 value that represents vertical offset from the left lower corner of pixel to left upper corner of pixel</param>
	/// <param name="backgroundColor">Color of the background used if Ray does not hit any Hitable objects</param>
	/// <returns>Color value in pixel after applying Random Antialiasing</returns>
	LightIntensity randomAntialiasing(Vector3 position, Vector3 leftLowerPixelCorner, const uint n, Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor);

	/// <summary>
	/// Implementation of Random Antialiasing
	/// </summary>
	/// <param name="position">Vector3 that represents position of the Camera</param>
	/// <param name="leftLowerPixelCorner">Vector3 value that represents left lower corner of Pixel</param>
	/// <param name="n">Size of matrix that represents pixel according to equasion n x n</param>
	/// <param name="horizontal">Vector3 value that represents horizontal offset from the left lower corner of pixel to right lower corner of pixel</param>
	/// <param name="vertical">Vector3 value that represents vertical offset from the left lower corner of pixel to left upper corner of pixel</param>
	/// <param name="backgroundColor">Color of the background used if Ray does not hit any Hitable objects</param>
	/// <returns>Color value in pixel after applying Random Antialiasing</returns>
	LightIntensity jitterAntialiasing(Vector3 position, Vector3 leftLowerPixelCorner, const uint n, Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor);

	/// <summary>
	/// Implementation of Poisson Disk Antialiasing
	/// </summary>
	/// <param name="position">Vector3 that represents position of the Camera</param>
	/// <param name="leftLowerPixelCorner">Vector3 value that represents left lower corner of Pixel</param>
	/// <param name="n">Size of samples taken</param>
	/// <param name="diff">Value of the difference used in determining if target should be marked again</param>
	/// <param name="horizontal">Vector3 value that represents horizontal offset from the left lower corner of pixel to right lower corner of pixel</param>
	/// <param name="vertical">Vector3 value that represents vertical offset from the left lower corner of pixel to left upper corner of pixel</param>
	/// <param name="backgroundColor">Color of the background used if Ray does not hit any Hitable objects</param>
	/// <returns>Color value in pixel after applying Poisson Disk Antialiasing</returns>
	LightIntensity poissonDiskAntialiasing(Vector3 position, Vector3 leftLowerPixelCorner, const uint n, const float diff, Vector3 horizontal, Vector3 vertical, const PixelColor& backgroundColor);
	
	Vector3 lowerLeftCorner;
	Vector3 horizontal, vertical;
	Vector3 pixelHorizontal, pixelVertical;
};