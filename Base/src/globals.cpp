#include "globals.h"
#include <cmath>

bool isFloatZero(float f)
{
	return std::fpclassify(f) == FP_ZERO;
}

bool isFloatNormal(float f)
{
	return std::fpclassify(f) == FP_NORMAL;
}