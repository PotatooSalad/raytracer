#include "Vector3.h"
#include "globals.h"
#include "Vector2.h"
#include "Vector4.h"
#include <math.h>
#include <exception>

const Vector3 Vector3::zero = Vector3(0.0f);
const Vector3 Vector3::one = Vector3(1.0f);
const Vector3 Vector3::up = Vector3(0.0f, 1.0f, 0.0f);

Vector3::Vector3(float newX, float newY, float newZ)
{
	x = newX;
	y = newY;
	z = newZ;
}

Vector3::Vector3(float newVal)
{
	x = y = z = newVal;
}

Vector3::Vector3(Vector2 v, float z)
{
	this->x = v.getX();
	this->y = v.getY();
	this->z = z;
}

Vector3::Vector3(Vector4 v)
{
	x = v.getX();
	y = v.getY();
	z = v.getZ();
}

Vector3::Vector3()
{
	*this = Vector3::zero;
}

float Vector3::getX()
{
	return x;
}

float Vector3::getY()
{
	return y;
}

float Vector3::getZ()
{
	return z;
}

void Vector3::setX(float newX)
{
	x = newX;
}

void Vector3::setY(float newY)
{
	y = newY;
}

void Vector3::setZ(float newZ)
{
	z = newZ;
}

void Vector3::setVector(float newX, float newY, float newZ)
{
	// Mo�e lepiej wywali� setX/Y/Z i zostawi� tego seta?
	setX(newX);
	setY(newY);
	setZ(newZ);
}

float Vector3::getLength()
{
	return sqrtf(x * x + y * y + z * z);
}

double Vector3::getLengthSquared()
{
	return ((double)x * x + (double)y * y + (double)z * z);
}

float Vector3::dot(Vector3 v)
{
	return (x * v.getX() + y * v.getY() + z * v.getZ());
}

Vector3 Vector3::cross(Vector3 v)
{
	return Vector3(y * v.getZ() - z * v.getY(),
				   z * v.getX() - x * v.getZ(),
				   x * v.getY() - y * v.getX());
}

Vector3 Vector3::normalized()
{
	return *this / getLength();
}

void Vector3::normalize()
{
	*this = *this / getLength();
}

Vector3 Vector3::reflect(Vector3 normal)
{
	Vector3 temp = Vector3(x, y, z);
	temp -= (2 * this->dot(normal) * normal);
	return temp;
}

Vector3 Vector3::vabs()
{
	return Vector3(fabs(x), fabs(y), fabs(z));
}

std::string Vector3::toString()
{
	std::string vectorToString = "";
	vectorToString = "(" + std::to_string(x) + ", " + std::to_string(y) + ", " + std::to_string(z) + ")";
	return vectorToString;
}

bool Vector3::equals(Vector3 other)
{
	bool areEquals = true;
	areEquals &= abs(this->x - other.x) < 0.001;
	areEquals &= abs(this->y - other.y) < 0.001;
	areEquals &= abs(this->z - other.z) < 0.001;
	return areEquals;
}

Vector3 Vector3::operator+(const Vector3& addend) const
{
	return Vector3(	this->x + addend.x, 
					this->y + addend.y, 
					this->z + addend.z);
}

Vector3 Vector3::operator-(const Vector3& subtrahend) const
{
	return Vector3(	this->x - subtrahend.x,
					this->y - subtrahend.y,
					this->z - subtrahend.z);
}

Vector3 Vector3::operator*(const float& factor) const
{
	return Vector3(	this->x * factor,
					this->y * factor,
					this->z * factor);
}

Vector3 Vector3::operator/(const float& divisor) const
{
	if (isFloatZero(divisor))
	{
		throw std::exception("Division by 0");
	}
	float inversed = 1.0f / divisor;

	return *this * inversed;
}

Vector3& Vector3::operator+=(const Vector3& addend)
{
	this->x += addend.x;
	this->y += addend.y;
	this->z += addend.z;
	return *this;
}

Vector3& Vector3::operator-=(const Vector3& subtrahend)
{
	this->x -= subtrahend.x;
	this->y -= subtrahend.y;
	this->z -= subtrahend.z;
	return *this;
}

Vector3& Vector3::operator*=(const float& factor)
{
	this->x *= factor;
	this->y *= factor;
	this->z *= factor;
	return *this;
}

Vector3& Vector3::operator/=(const float& divisor)
{
	if (isFloatZero(divisor))
	{
		throw std::exception("Division by 0");
	}
	float inversed = 1.0f / divisor;
	*this *= inversed;
	return *this;
}

Vector3 Vector3::operator-() const
{
	return Vector3(-x, -y, -z);
}

float Vector3::operator[](const int index) const
{
	switch (index)
	{
	case 0:
		return x;
	case 1:
		return y;
	case 2:
		return z;
	default:
		throw std::exception("Index out of bounds!");
	}
}

bool Vector3::isZero()
{
	if (isFloatZero(x) && isFloatZero(y) && isFloatZero(z))
	{
		return true;
	}
	return false;
}

std::ostream& operator<<(std::ostream& out, Vector3 vector)
{
	out << "(" << vector.getX() << ", " << vector.getY() << ",  " << vector.getZ() << ")";
	return out;
}
