#include "Vector4.h"
#include "Vector3.h"

Vector4::Vector4()
{
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
	w = 0.0f;
}

Vector4::Vector4(float newVal)
{
	x = newVal;
	y = newVal;
	z = newVal;
	w = newVal;
}

Vector4::Vector4(float newX, float newY, float newZ, float newW)
{
	x = newX;
	y = newY;
	z = newZ;
	w = newW;
}

Vector4::Vector4(Vector3 v, float newW)
{
	x = v.getX();
	y = v.getY();
	z = v.getZ();
	w = newW;
}

void Vector4::setX(float newX)
{
	x = newX;
}

void Vector4::setY(float newY)
{
	y = newY;
}

void Vector4::setZ(float newZ)
{
	z = newZ;
}

void Vector4::setW(float newW)
{
	w = newW;
}

float Vector4::getX()
{
	return x;
}

float Vector4::getY()
{
	return y;
}

float Vector4::getZ()
{
	return z;
}

float Vector4::getW()
{
	return w;
}

float& Vector4::operator[](int i)
{
	if (i > 3)
		throw std::exception("Tried to access variable outside of size");

	switch (i)
	{
	case 0:
		return x;
	case 1:
		return y;
	case 2:
		return z;
	case 3:
		return w;
	default:
		return x;
	}
}

Vector4 Vector4::operator+(const Vector4& addend) const
{
	return Vector4(x + addend.x, y + addend.y, z + addend.z, w + addend.w);
}

Vector4 Vector4::operator-(const Vector4& subtrahend) const
{
	return Vector4(x - subtrahend.x, y - subtrahend.y, z - subtrahend.z, w - subtrahend.w);
}

std::string Vector4::toString()
{
	std::string vectorToString = "";
	vectorToString = "(" + std::to_string(x) + ", " + std::to_string(y) + ", " + std::to_string(z) + ", " + std::to_string(w) + ")";
	return vectorToString;
}

std::ostream& operator<<(std::ostream& out, Vector4 vector)
{
	out << "(" << vector.getX() << ", " << vector.getY() << ",  " << vector.getZ() <<  vector.getW() << ")";
	return out;
}

float Vector4::dot(Vector4 other)
{
	return (x * other.x + y * other.y + z * other.z + w * other.w);
}
