#include "Scene.h"
#include "SceneObject.h"

SceneObject* Scene::makeAndGetNewSceneObject(Hitable* geometry, Material* material)
{
	scene.push_back(new SceneObject(geometry, material));
	return scene[scene.size() - 1];
}

void Scene::sortScene(const Vector3 point)
{
	throw std::exception("Unimplemented");
}

Scene::~Scene()
{
	for (auto obj : scene)
	{
		delete obj;
	}
	scene.clear();
}