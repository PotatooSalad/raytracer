#include "Matrix4x4.h"
#include "Vector3.h"
#include <iomanip>
#define PI 3.14159

const Matrix4x4 Matrix4x4::identity = Matrix4x4(
	Vector4(1.0f, 0.0f, 0.0f, 0.0f),
	Vector4(0.0f, 1.0f, 0.0f, 0.0f),
	Vector4(0.0f, 0.0f, 1.0f, 0.0f),
	Vector4(0.0f, 0.0f, 0.0f, 1.0f)
);

Matrix4x4::Matrix4x4()
{
	column[0] = Vector4(0.0f);
	column[1] = Vector4(0.0f);
	column[2] = Vector4(0.0f);
	column[3] = Vector4(0.0f);
}

Matrix4x4::Matrix4x4(float f)
{
	column[0] = Vector4(f);
	column[1] = Vector4(f);
	column[2] = Vector4(f);
	column[3] = Vector4(f);
}

Matrix4x4::Matrix4x4(Vector4 v1, Vector4 v2, Vector4 v3, Vector4 v4)
{
	column[0] = v1;
	column[1] = v2;
	column[2] = v3;
	column[3] = v4;
}

Vector4& Matrix4x4::operator[](int i)
{
	if (i > 3)
		throw std::exception("Tried to access variable outside of size");

	switch (i)
	{
	case 0:
		return column[0];
	case 1:
		return column[1];
	case 2:
		return column[2];
	case 3:
		return column[3];
	default:
		return column[0];
	}
}

Matrix4x4 Matrix4x4::transposed()
{
	Vector4 column1(column[0][0], column[1][0], column[2][0], column[3][0]);
	Vector4 column2(column[0][1], column[1][1], column[2][1], column[3][1]);
	Vector4 column3(column[0][2], column[1][2], column[2][2], column[3][2]);
	Vector4 column4(column[0][3], column[1][3], column[2][3], column[3][3]);
	return Matrix4x4(column1, column2, column3, column4);
}

Matrix4x4 Matrix4x4::operator*(Matrix4x4 other)
{
	Matrix4x4 tran = this->transposed();
	return Matrix4x4(
		Vector4(tran[0].dot(other[0]), tran[1].dot(other[0]), tran[2].dot(other[0]), tran[3].dot(other[0])),
		Vector4(tran[0].dot(other[1]), tran[1].dot(other[1]), tran[2].dot(other[1]), tran[3].dot(other[1])),
		Vector4(tran[0].dot(other[2]), tran[1].dot(other[2]), tran[2].dot(other[2]), tran[3].dot(other[2])),
		Vector4(tran[0].dot(other[3]), tran[1].dot(other[3]), tran[2].dot(other[3]), tran[3].dot(other[3]))
	);
}

Vector4 Matrix4x4::operator*(Vector4 v)
{
	Matrix4x4 tran = this->transposed();
	return Vector4(
		tran[0].dot(v),
		tran[1].dot(v),
		tran[2].dot(v),
		tran[3].dot(v)
	);
}

std::string Matrix4x4::toString()
{
	std::string temp;
	temp = "("
		+ std::to_string(column[0][0]) + ", " + std::to_string(column[1][0]) + ", " + std::to_string(column[2][0]) + ", " + std::to_string(column[3][0]) + ", " + '\n'
		+ std::to_string(column[0][1]) + ", " + std::to_string(column[1][1]) + ", " + std::to_string(column[2][1]) + ", " + std::to_string(column[3][1]) + ", " + '\n'
		+ std::to_string(column[0][2]) + ", " + std::to_string(column[1][2]) + ", " + std::to_string(column[2][2]) + ", " + std::to_string(column[3][2]) + ", " + '\n'
		+ std::to_string(column[0][3]) + ", " + std::to_string(column[1][3]) + ", " + std::to_string(column[2][3]) + ", " + std::to_string(column[3][3]) + ")";
	return temp;
}

Matrix4x4 Matrix4x4::translate(Vector3 v)
{
	Matrix4x4 temp = Matrix4x4(column[0], column[1], column[2], column[3]);

	temp[3] = column[3] + Vector4(v, 1.0f);
	return temp;
}

Matrix4x4 Matrix4x4::rotate(Vector3 axis, float angle)
{
	Matrix4x4 temp = Matrix4x4::identity;
	angle = angle * PI / 180.0f;
	axis.normalize();

	float sin = sinf(angle);
	float cos = cosf(angle);

	float cosm = (1 - cos);

	temp[0] = Vector4(
		axis.getX() * axis.getX() * cosm + cos,
		axis.getY() * axis.getX() * cosm + axis.getZ() * sin,
		axis.getX() * axis.getZ() * cosm - axis.getY() * sin,
		0.0
	);

	temp[1] = Vector4(
		axis.getX() * axis.getY() * cosm - axis.getZ() * sin,
		axis.getY() * axis.getY() * cosm + cos,
		axis.getY() * axis.getZ() * cosm + axis.getX() * sin,
		0.0
	);

	temp[2] = Vector4(
		axis.getX() * axis.getZ() * cosm + axis.getY() * sin,
		axis.getY() * axis.getZ() * cosm - axis.getX() * sin,
		axis.getZ() * axis.getZ() * cosm + cos,
		0.0
	);

	return *this * temp;
}

Matrix4x4 Matrix4x4::scale(Vector3 v)
{
	Matrix4x4 temp = Matrix4x4(column[0], column[1], column[2], column[3]);
	temp[0][0] = column[0][0] * v.getX();
	temp[1][1] = column[1][1] * v.getY();
	temp[2][2] = column[2][2] * v.getZ();
	temp[3][3] = column[3][3] * 1;
	return temp;
}

std::ostream& operator<<(std::ostream& out, Matrix4x4 column)
{
	out << "("
		<< column[0][0] << "," << column[1][0] << "," << column[2][0] << "," << column[3][0] << "," << std::endl
		<< column[0][1] << "," << column[1][1] << "," << column[2][1] << "," << column[3][1] << "," << std::endl
		<< column[0][2] << "," << column[1][2] << "," << column[2][2] << "," << column[3][2] << "," << std::endl
		<< column[0][3] << "," << column[1][3] << "," << column[2][3] << "," << column[3][3] << ")";

	return out;
}
