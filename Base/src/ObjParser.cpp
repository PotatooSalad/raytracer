#include "ObjParser.h"
#include <fstream>
#include <cmath>
#include <string>

void ObjParser::parseToMesh(const char* filename, std::vector<Vertex>& vertices, std::vector<uint>& indices)
{
	std::ifstream file(filename);
	std::string buffer;
	std::vector<Vector3> positions;
	std::vector<Vector3> normals;
	std::vector<Vector2> texCoords;
	bool normalFaces = true;
	if (file.good())
	{
		while (!file.eof())
		{
			std::getline(file, buffer);
			if (buffer[0] == '#' || buffer.size() < 5) // comment
				continue;
			if (buffer[0] == 'v' || buffer[0] == 'V')
			{
				if (buffer[1] == ' ') // v x y z
				{
					buffer = buffer.substr(2);

					float x = getFloatFromBuffer(buffer, ' ');
					float y = getFloatFromBuffer(buffer, ' ');
					float z = getFloatFromBuffer(buffer, ' ');

					positions.push_back(Vector3(x, y, z));
				}
				else if (buffer[1] == 't' || buffer[1] == 'T') //vt x y
				{
					buffer = buffer.substr(3);
					float x = getFloatFromBuffer(buffer, ' ');
					float y = getFloatFromBuffer(buffer, ' ');

					texCoords.push_back(Vector2(x, y));
				}
				else if (buffer[1] == 'n' || buffer[1] == 'N') //vn x y z
				{
					buffer = buffer.substr(3);
					float x = getFloatFromBuffer(buffer, ' ');
					float y = getFloatFromBuffer(buffer, ' ');
					float z = getFloatFromBuffer(buffer, ' ');

					normals.push_back(Vector3(x, y, z));
				}
			}
			else if (buffer[0] == 'f' || buffer[0] == 'F')
			{
				buffer = buffer.substr(2);
				if (buffer.find_first_of('/') != std::string::npos) // f v/vt/vn
				{
					normalFaces = false;
					while (buffer.find_first_of('/') != std::string::npos)
					{
						int v = -1, vt = -1, vn = -1;
						v = getIntFromBuffer(buffer, '/');

						if (buffer.find_first_of('/') != std::string::npos)
						{
							vt = getIntFromBuffer(buffer, '/');
						}
						else // v//nv
						{
							buffer = buffer.substr(1);
						}

						vn = getIntFromBuffer(buffer, ' ');

						vertices.push_back(Vertex(positions[v - 1]));
						vertices[vertices.size() - 1].normal = normals[vn - 1];
						if (vt != -1 && texCoords.size() > 0)
						{
							vertices[vertices.size() - 1].texCoord = texCoords[vt - 1];
						}
					}
				}
				else // f v v v
				{
					size_t space = buffer.find_first_of(' ');
					int v1 = std::stoi(buffer.substr(0, space));
					buffer = buffer.substr(space + 1);

					space = buffer.find_first_of(' ');
					int v2 = std::stoi(buffer.substr(0, space));
					buffer = buffer.substr(space + 1);
					int v3 = -1, v4 = -1;

					if (buffer.find_first_of(' ') != std::string::npos)
					{
						space = buffer.find_first_of(' ');
						v3 = std::stoi(buffer.substr(0, space));
						buffer = buffer.substr(space + 1);

						v4 = std::stoi(buffer);

						indices.push_back(v1 - 1);
						indices.push_back(v2 - 1);
						indices.push_back(v4 - 1);
						indices.push_back(v1 - 1);
						indices.push_back(v4 - 1);
						indices.push_back(v3 - 1);
					}
					else
					{
						v3 = std::stoi(buffer);
						indices.push_back(v1 - 1);
						indices.push_back(v2 - 1);
						indices.push_back(v3 - 1);
					}
				}
			}
		}
	}
	file.close();

	for (int i = 0; i < positions.size() && normalFaces; ++i)
	{
		vertices.push_back(Vertex(positions[i]));
		if (normals.size() > 0)
		{
			vertices[vertices.size() - 1].normal = normals[i];
		}
		if (texCoords.size() > 0)
		{
			vertices[vertices.size() - 1].texCoord = texCoords[i];
		}
	}
}

float ObjParser::getFloatFromBuffer(std::string& buffer, char splitChar)
{
	float temp = INFINITY;
	size_t splitPos;
	while (fpclassify(temp) == FP_INFINITE)
	{
		try
		{
			splitPos = buffer.find_first_of(splitChar);
			temp = std::stof(buffer.substr(0, splitPos));
			buffer = buffer.substr(splitPos + 1);
		}
		catch (std::invalid_argument)
		{
			buffer = buffer.substr(splitPos + 1);
			temp = INFINITY;
		}
	}
	return temp;
}

int ObjParser::getIntFromBuffer(std::string& buffer, char splitChar)
{
	int temp = -1;
	size_t splitPos;
	try
	{
		splitPos = buffer.find_first_of(splitChar);
		temp = std::stoi(buffer.substr(0, splitPos));
		buffer = buffer.substr(splitPos + 1);
	}
	catch (std::invalid_argument)
	{
		buffer = buffer.substr(splitPos + 1);
		temp = -1;
	}
	return temp;
}

void ObjParser::parseToMeshesWithMaterials(const char* filename, std::vector<Mesh*>& meshes, std::vector<Material*>& materials, Matrix4x4 objToWorldMatrix)
{
	std::ifstream file(filename);
	std::string buffer;
	std::vector<Vector3> positions;
	std::vector<Vector3> normals;
	std::vector<Vector2> texCoords;
	std::vector<Vertex> vertices;
	std::vector<uint> indices;
	bool normalFaces = true;

	Material* lastMaterial = nullptr;
	if (file.good())
	{
		while (!file.eof())
		{
			std::getline(file, buffer);
			if (buffer[0] == '#' || buffer.size() < 5) // comment
				continue;
			if (buffer[0] == 'v' || buffer[0] == 'V')
			{
				if (buffer[1] == ' ') // v x y z
				{
					buffer = buffer.substr(2);

					float x = getFloatFromBuffer(buffer, ' ');
					float y = getFloatFromBuffer(buffer, ' ');
					float z = getFloatFromBuffer(buffer, ' ');

					positions.push_back(Vector3(x, y, z));
				}
				else if (buffer[1] == 't' || buffer[1] == 'T') //vt x y
				{
					buffer = buffer.substr(3);
					float x = getFloatFromBuffer(buffer, ' ');
					float y = getFloatFromBuffer(buffer, ' ');

					texCoords.push_back(Vector2(x, y));
				}
				else if (buffer[1] == 'n' || buffer[1] == 'N') //vn x y z
				{
					buffer = buffer.substr(3);
					float x = getFloatFromBuffer(buffer, ' ');
					float y = getFloatFromBuffer(buffer, ' ');
					float z = getFloatFromBuffer(buffer, ' ');

					normals.push_back(Vector3(x, y, z));
				}
			}
			else if (buffer[0] == 'f' || buffer[0] == 'F')
			{
				buffer = buffer.substr(2);
				if (buffer.find_first_of('/') != std::string::npos) // f v/vt/vn
				{
					normalFaces = false;
					while (buffer.find_first_of('/') != std::string::npos)
					{
						int v = -1, vt = -1, vn = -1;
						v = getIntFromBuffer(buffer, '/');

						if (buffer.find_first_of('/') != std::string::npos)
						{
							vt = getIntFromBuffer(buffer, '/');
						}
						else // v//nv
						{
							buffer = buffer.substr(1);
						}

						vn = getIntFromBuffer(buffer, ' ');

						vertices.push_back(Vertex(positions[v - 1]));
						vertices[vertices.size() - 1].normal = normals[vn - 1];
						if (vt != -1)
						{
							vertices[vertices.size() - 1].texCoord = texCoords[vt - 1];
						}
					}
				}
				else // f v v v
				{
					size_t space = buffer.find_first_of(' ');
					int v1 = std::stoi(buffer.substr(0, space));
					buffer = buffer.substr(space + 1);

					space = buffer.find_first_of(' ');
					int v2 = std::stoi(buffer.substr(0, space));
					buffer = buffer.substr(space + 1);
					int v3 = -1, v4 = -1;

					if (buffer.find_first_of(' ') != std::string::npos)
					{
						space = buffer.find_first_of(' ');
						v3 = std::stoi(buffer.substr(0, space));
						buffer = buffer.substr(space + 1);

						v4 = std::stoi(buffer);

						indices.push_back(v1 - 1);
						indices.push_back(v2 - 1);
						indices.push_back(v4 - 1);
						indices.push_back(v1 - 1);
						indices.push_back(v4 - 1);
						indices.push_back(v3 - 1);
					}
					else
					{
						v3 = std::stoi(buffer);
						indices.push_back(v1 - 1);
						indices.push_back(v2 - 1);
						indices.push_back(v3 - 1);
					}
				}
			}
			else if (buffer[0] == 'u' && buffer[1] == 's')
			{

				size_t space = buffer.find_first_of(' ');
				std::string name = buffer.substr(space + 1);
				for (int i = 0; i < materials.size(); ++i)
				{
					if (materials[i]->getName().compare(name) == 0)
					{
						lastMaterial = materials[i];
					}
				}
			}
			else if (buffer[0] == 'o' || buffer[0] == 'O')
			{
				if (positions.size() == 0)
					continue;
				for (int i = 0; i < positions.size() && normalFaces; ++i)
				{
					vertices.push_back(Vertex(positions[i]));
					if (normals.size() > 0)
					{
						vertices[vertices.size() - 1].normal = normals[i];
					}
					if (texCoords.size() > 0)
					{
						vertices[vertices.size() - 1].texCoord = texCoords[i];
					}
					
				}
				meshes.push_back(new Mesh(vertices, indices, objToWorldMatrix, lastMaterial));
				vertices.clear();
				indices.clear();
			}
			else if (buffer[0] == 'm' && buffer[1] == 't' && buffer[2] == 'l')
			{
				size_t space = buffer.find_first_of(' ');
				std::string name = buffer.substr(space + 1);
				parseMaterials(name.c_str(), materials);
			}
		}
		for (int i = 0; i < positions.size() && normalFaces; ++i)
		{
			vertices.push_back(Vertex(positions[i]));
			if (normals.size() > 0)
			{
				vertices[vertices.size() - 1].normal = normals[i];
			}
			if (texCoords.size() > 0)
			{
				vertices[vertices.size() - 1].texCoord = texCoords[i];
			}
		}
		meshes.push_back(new Mesh(vertices, indices, objToWorldMatrix, lastMaterial));
	}
	file.close();
}


void ObjParser::parseMaterials(const char* filename, std::vector<Material*>& materials)
{
	std::ifstream file(filename);
	std::string buffer;
	std::string name;
	LightIntensity ambient(0.0);
	LightIntensity diffuse(0.0);
	float specular = 1.0f;
	bool write = false;
	if (file.good())
	{
		while (!file.eof())
		{
			std::getline(file, buffer);
			if (buffer[0] == 'n' && buffer[1] == 'e')
			{
				if (write)
				{
					materials.push_back(new Material(name, diffuse, specular));
				}
				size_t space = buffer.find_first_of(' ');
				name = buffer.substr(space + 1);
			}
			else if (buffer[0] == 'K')
			{
				if (buffer[1] == 'd') //diffuse
				{
					float r = getFloatFromBuffer(buffer, ' ');
					float g = getFloatFromBuffer(buffer, ' ');
					float b = getFloatFromBuffer(buffer, ' ');
					diffuse = LightIntensity(r, g, b);
				}
				else if (buffer[1] == 's') //specular
				{
					float r = getFloatFromBuffer(buffer, ' ');
					specular = r;
				}
			}
		}
		materials.push_back(new Material(name, diffuse, specular));
	}
}