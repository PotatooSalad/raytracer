#include "SceneObject.h"
#include "Hitable.h"
#include "Material.h"
#include "Vector3.h"
#include "Vector2.h"
#include "Texture.h"
#include "PixelColor.h"

SceneObject::SceneObject(Hitable* geometry, Material* material)
{
	this->geometry = geometry;
	this->material = material;
}

LightIntensity SceneObject::getObjectDiffuse(Vector3 point)
{
	LightIntensity matDiffuse = material->getDiffuse();
	if (material->getTexture() != nullptr && material->getMaterialType() == MaterialType::NORMAL)
	{
		Vector2 uv = geometry->calculateUVCoordinates(point);
		matDiffuse = material->getTexture()->extractTextureColor(uv);
		matDiffuse = matDiffuse * LightIntensity(material->getTexture()->extractTextureColor(uv));
		matDiffuse.clampValues();
	}
	return matDiffuse;
}