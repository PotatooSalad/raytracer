#include "Vector2.h"
#include "Vector3.h"
#include "globals.h"

const Vector2 Vector2::zero = Vector2(0.0f);
const Vector2 Vector2::one = Vector2(1.0f);

Vector2::Vector2()
{
	x = y = 0.0f;
}

Vector2::Vector2(float x, float y)
{
	this->x = x;
	this->y = y;
}

Vector2::Vector2(Vector3 v)
{
	this->x = v.getX();
	this->y = v.getY();
}

Vector2::Vector2(float x)
{
	this->x = this->y = x;
}

float Vector2::getLength()
{
	return sqrtf(x * x + y * y);
}

float Vector2::getLengthSquared()
{
	return (x * x + y * y);
}

Vector2 Vector2::normalized()
{
	return *this / getLength();
}

bool Vector2::equals(Vector2 other)
{
	bool areEquals = true;
	areEquals &= abs(this->x - other.x) < 0.001;
	areEquals &= abs(this->y - other.y) < 0.001;
	return areEquals;
}

Vector2 Vector2::operator+(const Vector2& addend) const
{
	return Vector2(x + addend.x, y + addend.y);
}

Vector2 Vector2::operator-(const Vector2& subtrahend) const
{
	return Vector2(x - subtrahend.x, y - subtrahend.y);
}

Vector2 Vector2::operator*(const float& factor) const
{
	return Vector2(x * factor, y * factor);
}

Vector2 Vector2::operator/(const float& divisor) const
{
	if (isFloatZero(divisor))
	{
		throw std::exception("Division by 0!");
	}
	float fac = 1.0f / divisor;
	return Vector2(x * fac, y * fac);
}

Vector2& Vector2::operator+=(const Vector2& addend)
{
	this->x += addend.x;
	this->y += addend.y;
	return *this;
}

Vector2& Vector2::operator-=(const Vector2& subtrahend)
{
	this->x -= subtrahend.x;
	this->y -= subtrahend.y;
	return *this;
}

Vector2& Vector2::operator*=(const float& factor)
{
	this->x *= factor;
	this->y *= factor;
	return *this;
}

Vector2& Vector2::operator/=(const float& divisor)
{
	if (isFloatZero(divisor))
	{
		throw std::exception("Division by 0!");
	}
	float fac = 1.0f / divisor;
	*this *= fac;
	return *this;
}

Vector2 Vector2::operator-() const
{
	return Vector2(-x, -y);
}

bool Vector2::isZero()
{
	return isFloatZero(x) && isFloatZero(y);
}

std::ostream& operator<< (std::ostream& out, Vector2 vector)
{
	out << "(" << vector.getX() << ", " << vector.getY() << ")";

	return out;
}