#include "Intersection.h"

std::ostream& operator<< (std::ostream& out, Intersection interEnum)
{
	switch (interEnum)
	{
	case Intersection::MISS:
		out << "MISS";
		break;
	case Intersection::SINGLE_POINT:
		out << "SINGLE_POINT";
		break;
	case Intersection::TWO_POINTS:
		out << "TWO_POINTS";
		break;
	default:
		out << "ERROR";
		break;
	}
	return out;
}