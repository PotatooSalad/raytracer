#pragma once
#include "Mesh.h"
#include "Material.h"

/// <summary>
/// Parse OBJ file class
/// </summary>
class ObjParser
{
public:
	/// <summary>
	/// Default constructor of class
	/// </summary>
	ObjParser() = default;

	/// <summary>
	/// Parse OBJ file to Mesh object
	/// </summary>
	/// <param name="filename">Name of obj file</param>
	/// <param name="vertices">Reference to vertices vector</param>
	/// <param name="indices">Reference to indices vector</param>
	/// <returns>Mesh object which contains OBJ file data</returns>
	void parseToMesh(const char* filename, std::vector<Vertex>& vertices, std::vector<uint>& indices);

	/// <summary>
	/// Parse OBJ file with MTL file with multiple meshes
	/// </summary>
	/// <param name="filename">Name of obj file</param>
	/// <param name="meshes">Reference to vector of meshes (for return)</param>
	/// <param name="materials">Reference to vector of materials (for return and assignment)</param>
	/// <param name="objToWorldMatrix">Matrix object to world (for Mesh constructor)</param>
	void parseToMeshesWithMaterials(const char* filename, std::vector<Mesh*>& meshes, std::vector<Material*>& materials, Matrix4x4 objToWorldMatrix);

private:
	/// <summary>
	/// Support function that gets first float from string buffer
	/// </summary>
	/// <param name="buffer">String buffer with float</param>
	/// <param name="splitChar">Character that splits floats</param>
	/// <returns>Float value parsed by std::stof function</returns>
	float getFloatFromBuffer(std::string& buffer, char splitChar);

	/// <summary>
	/// Support function that gets first int from string buffer
	/// </summary>
	/// <param name="buffer">String buffer with int</param>
	/// <param name="splitChar">Character that splits floats</param>
	/// <returns>Int value parsed by std::stoi function</returns>
	int getIntFromBuffer(std::string& buffer, char splitChar);

	/// <summary>
	/// Parse MTL file to material pointer vector
	/// </summary>
	/// <param name="filename">Name of MTL file</param>
	/// <param name="materials">Reference to vector of materials (for return)</param>
	void parseMaterials(const char* filename, std::vector<Material*>& materials);
};

