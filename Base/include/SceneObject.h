#pragma once

class Material;
class Hitable;
class Vector3;
class LightIntensity;

/// <summary>
/// Class that binds geometry with material - Objects on Scene
/// </summary>
class SceneObject
{
public:
	friend class Scene;
	friend class Mesh;
	/// <summary>
	/// Geometry of Scene Object getter
	/// </summary>
	/// <returns>Pointer to Hitable object which defines geometry of SceneObject</returns>
	Hitable* getGeometry() { return geometry; }

	/// <summary>
	/// Material of SceneObject getter
	/// </summary>
	/// <returns>Pointer to Material object which defines material of SceneObject</returns>
	Material* getMaterial() { return material; }

	/// <summary>
	/// Setter of SceneObject material
	/// </summary>
	/// <param name="material">Material pointer which will be assigned to material</param>
	void setMaterial(Material* material) { this->material = material; }

	/// <summary>
	/// Setter of SceneObject geometry
	/// </summary>
	/// <param name="geometry">Hitable pointer which will be assinged to geometry</param>
	void setGeometry(Hitable* geometry) { this->geometry = geometry; }

	/// <summary>
	/// Get object diffuse color in point
	/// </summary>
	/// <param name="point">Point in geometry wrom which we want diffuse color</param>
	/// <returns>Difuse color for that point</returns>
	LightIntensity getObjectDiffuse(Vector3 point);

private:
	/// <summary>
	/// Scene Object class constructor
	/// </summary>
	/// <param name="geometry">Pointer for object geometry</param>
	/// <param name="material">Pointer for object material</param>
	SceneObject(Hitable* geometry, Material* material);

	/// <summary>
	/// Pointer for Hitable object - geometry of Scene Object
	/// </summary>
	Hitable* geometry = nullptr;

	/// <summary>
	/// Pointer for Material object - material of Scene Object
	/// </summary>
	Material* material = nullptr;
};

