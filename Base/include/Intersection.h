#pragma once
#include <ostream>

/// <summary>
/// Enumerator used to describe amount of intersection points
/// </summary>
enum class Intersection : unsigned int
{
	MISS,
	SINGLE_POINT,
	TWO_POINTS
};

/// <summary>
/// Overloaded operator for Intersection enum (aka "toString)
/// </summary>
/// <param name="out">Handle to ostream object</param>
/// <param name="interEnum">Enum which will be made to string</param>
/// <returns> Handle to output stream with string enum data </returns>
std::ostream& operator<< (std::ostream& out, Intersection interEnum);