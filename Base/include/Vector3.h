#pragma once
#include <string>
#include <ostream>
class Vector2;
class Vector4;

/// <summary>
/// Implementation of 3D vector (or point)
/// </summary>
class Vector3
{
public:
	/// <summary>
	/// Default constructor for Vector3. All coordinates are zeros.
	/// </summary>
	Vector3();

	/// <summary>
	/// Constructor for Vector3 that makes all coordinates the same
	/// </summary>
	/// <param name="newVal">Value that will be assigned to all three coordinates</param>
	Vector3(float newVal);

	/// <summary>
	/// Constructor for Vector3
	/// </summary>
	/// <param name="newX">Float value assigned to x</param>
	/// <param name="newY">Float value assigned to y</param>
	/// <param name="newZ">Float value assigned to z</param>
	Vector3(float newX, float newY, float newZ);	

	/// <summary>
	/// Constructor for vector3 with Vector2
	/// </summary>
	/// <param name="v">Vector2 object</param>
	/// <param name="z">Value of z</param>
	Vector3(Vector2 v, float z);

	/// <summary>
	/// Constructor for vector3 with Vector4
	/// </summary>
	/// <param name="v">Vector4 object</param>
	Vector3(Vector4 v);
	
	/// <summary>
	/// Setter of float value x
	/// </summary>
	/// <param name="newX">New value of x</param>
	void setX(float newX);

	/// <summary>
	/// Setter of float value y
	/// </summary>
	/// <param name="newY">New value of y</param>
	void setY(float newY);

	/// <summary>
	/// Setter of float value z
	/// </summary>
	/// <param name="newZ">New value of z</param>
	void setZ(float newZ);

	/// <summary>
	/// Sets new float values of Vector 3D
	/// </summary>
	/// <param name="newX">New value of x</param>
	/// <param name="newY">New value of y</param>
	/// <param name="newZ">New value of z</param>
	void setVector(float newX, float newY, float newZ);

	/// <summary>
	/// Getter of float value x
	/// </summary>
	/// <returns>X coordinate of Vector3</returns>
	float getX();

	/// <summary>
	/// Getter of float value y
	/// </summary>
	/// <returns>Y coordinate of Vector3</returns>
	float getY();

	/// <summary>
	/// Getter of float value z
	/// </summary>
	/// <returns>Z coordinate of Vector3</returns>
	float getZ();

	/// <summary>
	/// Getter of length of the Vector3
	/// </summary>
	/// <returns>Float value of length of the vector</returns>
	float getLength();

	/// <summary>
	/// Getter of square of length of the Vector3
	/// </summary>
	/// <returns>Float value of square of length of the vector</returns>
	double getLengthSquared();

	/// <summary>
	/// Dot product
	/// </summary>
	/// <param name="v">Second Vector3 used in the multiplication</param>
	/// <returns>Float value of dot multiplication</returns>
	float dot(Vector3 v);

	/// <summary>
	/// Cross product of the multiplication
	/// </summary>
	/// <param name="v">Second Vector3 used in the multiplication</param>
	/// <returns>Vector3 value of cross multiplication</returns>
	Vector3 cross(Vector3 v);

	/// <summary>
	/// Normalization of the Vector3
	/// </summary>
	void normalize();

	/// <summary>
	/// Normalization of the Vector3
	/// </summary>
	/// <returns>Return new Vector3 which is normalized from this</returns>
	Vector3 normalized();

	/// <summary>
	/// Reflects a vector off the plane defined by a normal
	/// </summary>
	/// <param name="normal">Plane defined as normal Vector3</param>
	/// <returns>Return new Vector3 which is Vector3 after reflection</returns>
	Vector3 reflect(Vector3 normal);

	/// <summary>
	/// Absolute value of vector3
	/// </summary>
	/// <returns>Vector3 with all coordinates absolute</returns>
	Vector3 vabs();

	/// <summary>
	/// Vector3 to string
	/// </summary>
	/// <returns>String consisting of x, y, z values</returns>
	std::string toString();

	/// <summary>
	/// Comparison function for two Vector3
	/// </summary>
	/// <param name="other">Other Vector3 for comparison</param>
	/// <returns>True if all three coordinates are approximately equals (epsilon = 0.001).
	/// False, if they are not</returns>
	bool equals(Vector3 other);

	/// <summary>
	/// Addition operator of two Vectors
	/// </summary>
	/// <param name="addend">Addend of operation</param>
	/// <returns>New Vector3 object which is sum of this and addend</returns>
	Vector3 operator+(const Vector3& addend) const;

	/// <summary>
	/// Subtraction operator of two Vectors
	/// </summary>
	/// <param name="subtrahend">Subtrahend of operation</param>
	/// <returns>New Vector3 object which is difference of this and subtrahend</returns>
	Vector3 operator-(const Vector3& subtrahend) const;

	/// <summary>
	/// Multiplication operator of Vector3 and float
	/// </summary>
	/// <param name="factor">Factor of operation</param>
	/// <returns>New Vector3 object which is product of this and factor</returns>
	Vector3 operator*(const float& factor) const;

	/// <summary>
	/// Division operator of Vector3 and float
	/// </summary>
	/// <param name="divisor">Divisor of operation</param>
	/// <exception cref="std::exception">Thrown when divider is zero</exception>
	/// <returns>New Vector3 object which is quotient of this and divisor</returns>
	Vector3 operator/(const float& divisor) const;

	/// <summary>
	/// Addition with assignment operator of two Vectors
	/// </summary>
	/// <param name="addend">Addend of operation</param>
	/// <returns>Reference to this object after addition the addend to this</returns>
	Vector3& operator+=(const Vector3& addend);

	/// <summary>
	/// Subtraction with assignment operator of two Vectors
	/// </summary>
	/// <param name="subtrahend">Subtrahend of operation</param>
	/// <returns>Reference to this object after subtraction the subtrahend from this</returns>
	Vector3& operator-=(const Vector3& subtrahend);

	/// <summary>
	/// Multiplication with assignment operator of Vector3 and float
	/// </summary>
	/// <param name="factor">Factor of operation</param>
	/// <returns>Reference to this object after multiplication the factor to this</returns>
	Vector3& operator*=(const float& factor);

	/// <summary>
	/// Division with assignment operator of Vector3 and float
	/// </summary>
	/// <param name="divisor">Divisor of operation</param>
	/// <returns>Reference to this object after division of this by divisor</returns>
	/// <exception cref="std::exception">Thrown when divider is zero</exception>
	Vector3& operator/=(const float& divisor);

	/// <summary>
	/// Left-side negation operator
	/// </summary>
	/// <returns>New Vector3 object with negated coordinates from this</returns>
	Vector3 operator-() const;

	/// <summary>
	/// Table operator for Vector3
	/// </summary>
	/// <param name="index">Table index (less than 3)</param>
	/// <returns>Vector3 coordinate according to index</returns>
	float operator[](const int index) const;

	/// <summary>
	/// Vector3 initialized with zeros
	/// </summary>
	static const Vector3 zero;

	/// <summary>
	/// Vector3 initialized with ones
	/// </summary>
	static const Vector3 one;

	/// <summary>
	/// Vector3 initialized with y value equal to 1.0f
	/// </summary>
	static const Vector3 up;

	/// <summary>
	/// Checks if vector is all zeros
	/// </summary>
	/// <returns>True if vector is zero, false - if it's not</returns>
	bool isZero();

private:
	/// <summary>
	/// X coordinate
	/// </summary>
	float x;
	/// <summary>
	/// Y coordinate
	/// </summary>
	float y;
	/// <summary>
	/// Z coordinate
	/// </summary>
	float z;
};

/// <summary>
/// Multiplication operator of float and Vector3
/// </summary>
/// <param name="factor">Float factor of operation</param>
/// <param name="vfactor">Vector3 factor of operation</param>
/// <returns>New Vector3 object which is product of factor and vfactor</returns>
static Vector3 operator*(const float& factor, const Vector3 vfactor)
{
	return vfactor * factor;
}

/// <summary>
/// Overloaded operator for Vector3 class (aka "toString")
/// </summary>
/// <param name="out">Handle to ostream object</param>
/// <param name="vector">Vector3 which will be made to string</param>
/// <returns> Handle to output stream with string Vector3 data </returns>
std::ostream& operator<< (std::ostream& out, Vector3 vector);
