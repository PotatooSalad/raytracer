#pragma once
#include "Vector4.h"

class Vector3;

/// <summary>
/// Implementation of float Matrix4x4
/// </summary>
class Matrix4x4
{
public:
	/// <summary>
	/// Default constructor of Matrix4x4 that assigns all fields as 0.0f
	/// </summary>
	Matrix4x4();

	/// <summary>
	/// constructor of Matrix4x4 that assigns all fields as Float f
	/// </summary>
	/// <param name="f">Float f that all fields are assigned</param>
	Matrix4x4(float f);

	/// <summary>
	/// Constructor of Matrix4x4 that assigns all fields as specific Vector4
	/// </summary>
	/// <param name="v1">Vector4 assigned to matrix[0]</param>
	/// <param name="v2">Vector4 assigned to matrix[1]</param>
	/// <param name="v3">Vector4 assigned to matrix[2]</param>
	/// <param name="v4">Vector4 assigned to matrix[3]</param>
	Matrix4x4(Vector4 v1, Vector4 v2, Vector4 v3, Vector4 v4);

	/// <summary>
	/// Constructor of Matrix4x4 that creates identity matrix
	/// </summary>
	static const Matrix4x4 identity;

	/// <summary>
	/// Overloaded subscript operator used to access to specific field of the Matrix4x
	/// </summary>
	/// <param name="i">Int value that is used as an index of matrix array</param>
	/// <returns>Reference to specific Vector4</returns>
	Vector4& operator[](int i);

	/// <summary>
	/// Matrix multiplication operator of two Matrix4x4
	/// </summary>
	/// <param name="other">Second Matrix4x4</param>
	/// <returns>Matrix4x4 that is the result of multiplication of this matrix and m</returns>
	Matrix4x4 operator*(Matrix4x4 other);

	/// <summary>
	/// Matrix multiplication operator of this Matrix4x4 and Vector4
	/// </summary>
	/// <param name="v">Vector4 that this Matrix is multiplied</param>
	/// <returns>Float4 that is the result of multiplication of this matrix and Vector4</returns>
	Vector4 operator*(Vector4 v);

	/// <summary>
	/// Matrix4x4 to string
	/// </summary>
	/// <returns>String made out of matrix in column order</returns>
	std::string toString();

	/// <summary>
	/// Translates Matrix by a Vector3
	/// </summary>
	/// <param name="v">Translation Vector</param>
	/// <returns>Translated Matrix4x4</returns>
	Matrix4x4 translate(Vector3 v);

	/// <summary>
	/// Rotates Matrix by angle along rotation axis
	/// </summary>
	/// <param name="axis">Axis along object will be rotated</param>
	/// <param name="angle">Angle of rotation (in degrees)</param>
	/// <returns>Rotated Matrix4x4</returns>
	Matrix4x4 rotate(Vector3 axis, float angle);

	/// <summary>
	/// Sclaes Matrix by a Vector3
	/// </summary>
	/// <param name="v">Scale Vector3</param>
	/// <returns>Scaled Matrix4x4</returns>
	Matrix4x4 scale(Vector3 v);

	/// <summary>
	/// Transpose matrix
	/// </summary>
	/// <returns>Transposed matrix</returns>
	Matrix4x4 transposed();

private:
	/// <summary>
	/// Vector4 array of size 4 represents columns
	/// </summary>
	Vector4 column[4];
};

/// <summary>
/// Overloaded operator for Matrix4x4 class (aka "toString")
/// </summary>
/// <param name="out">Handle to ostream object</param>
/// <param name="matrix">Matrix4x4 which will be made to string</param>
/// <returns> Handle to output stream with string Matrix4x4 data </returns>
std::ostream& operator<< (std::ostream& out, Matrix4x4 column);