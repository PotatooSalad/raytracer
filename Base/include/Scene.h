#pragma once
#include <vector>
#include "Vector3.h"

class SceneObject;
class Hitable;
class Material;
class Light;
class DirectionalLight;
class Mesh;

/// <summary>
/// Scene class
/// </summary>
class Scene
{
public:
	/// <summary>
	/// Scene default constructor
	/// </summary>
	Scene() = default;

	/// <summary>
	/// Scene default destructor
	/// </summary>
	~Scene();

	/// <summary>
	/// Makes a new Scene Object, saves it to scene vector and returns that SceneObject
	/// </summary>
	/// <param name="geometry">Pointer for object geometry</param>
	/// <param name="material">Pointer for object material</param>
	/// <returns>Pointer to new SceneObject saved in scene vector</returns>
	SceneObject* makeAndGetNewSceneObject(Hitable* geometry, Material* material);

	/// <summary>
	/// Adds mesh to scene
	/// </summary>
	/// <param name="mesh">Mesh to add</param>
	void addMesh(Mesh* mesh) { meshes.push_back(mesh); }

	/// <summary>
	/// Add point or spot light to scene
	/// </summary>
	/// <param name="light">Pointer to Pointlight or SpotLight object</param>
	void addOtherLight(Light* light) { lights.push_back(light); }

	/// <summary>
	/// Scene point or spot lights getter
	/// </summary>
	/// <returns>Lights of the scene</returns>
	std::vector<Light*>* getLights() { return &lights; }

	/// <summary>
	/// Sets directional light for scene
	/// </summary>
	/// <param name="light">Directional Light to add to scene</param>
	void setDirectional(DirectionalLight* light) { directionalLight = light; }

	/// <summary>
	/// Getter for directional light of scene
	/// </summary>
	/// <returns>Directional light of scene</returns>
	DirectionalLight* getDirectional() { return directionalLight; }

	/// <summary>
	/// Scene vector getter
	/// </summary>
	/// <returns>Pointer to vector of pointers of SceneObject</returns>
	std::vector<SceneObject*>* getScene() { return &scene; }

	/// <summary>
	/// Scene meshes vector getter
	/// </summary>
	/// <returns>Pointer to vector of pointers of Mesh</returns>
	std::vector<Mesh*>* getSceneMeshes() { return &meshes; }
private:
	/// <summary>
	/// Vector of SceneObject pointers - scene
	/// </summary>
	std::vector<SceneObject*> scene;

	/// <summary>
	/// Vector pf meshes in scene
	/// </summary>
	std::vector<Mesh*> meshes;

	/// <summary>
	/// Scene point lights
	/// </summary>
	std::vector<Light*> lights;

	/// <summary>
	/// Scene directional (for debug)
	/// </summary>
	DirectionalLight* directionalLight = nullptr;

	/// <summary>
	/// WIP: DO NOT USE!
	/// Method that sorts all object in scene by its distance from point
	/// </summary>
	/// <param name="point">Reference point from which it will be counted distance</param>
	void sortScene(const Vector3 point);
};

