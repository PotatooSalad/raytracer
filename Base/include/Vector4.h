#pragma once
#include <string>
class Vector3;

/// <summary>
/// Implementation of 4D vector
/// </summary>
class Vector4
{
public:
	/// <summary>
	/// Default constructor of Vector4 that sets all fields as 0.0f
	/// </summary>
	Vector4();

	/// <summary>
	/// Constructor of Vector4 that sets all fields as newVal
	/// </summary>
	/// <param name="newVal">Float value that is assigned to all fields</param>
	Vector4(float newVal);

	/// <summary>
	/// Constructor of Vector4 that sets all fields individually
	/// </summary>
	/// <param name="newX">Float value of X</param>
	/// <param name="newY">Float value of Y</param>
	/// <param name="newZ">Float value of Z</param>
	/// <param name="newW">Float value of W</param>
	Vector4(float newX, float newY, float newZ, float newW);

	/// <summary>
	/// Constructor of Vector4 that uses Vector3 and float to assign fields
	/// </summary>
	/// <param name="v">Vector3 used in assigning x, y and z</param>
	/// <param name="w">Float value of W</param>
	Vector4(Vector3 v, float w);

	/// <summary>
	/// Setter of x
	/// </summary>
	/// <param name="newX">Float value that is assigned to X</param>
	void setX(float newX);

	/// <summary>
	/// Setter of y
	/// </summary>
	/// <param name="newY">Float value that is assigned to Y</param>
	void setY(float newY);

	/// <summary>
	/// Setter of z
	/// </summary>
	/// <param name="newZ">Float value that is assigned to Z</param>
	void setZ(float newZ);

	/// <summary>
	/// Setter of w
	/// </summary>
	/// <param name="newW">Float value that is assigned to W</param>
	void setW(float newW);

	/// <summary>
	/// Getter of x
	/// </summary>
	/// <returns>Float value of X</returns>
	float getX();

	/// <summary>
	/// Getter of y
	/// </summary>
	/// <returns>Float value of Y</returns>
	float getY();

	/// <summary>
	/// Getter of Z
	/// </summary>
	/// <returns>Float value of Z</returns>
	float getZ();

	/// <summary>
	/// Getter of W
	/// </summary>
	/// <returns>Float value of W</returns>
	float getW();

	/// <summary>
	/// Overloaded subscript operator used to access to specific field of the Vector4
	/// </summary>
	/// <param name="i">Int value that is used as an index of abstract array where
	/// 0 is x, 1 is y, 2 is z, w is 3</param>
	/// <returns>Reference to specific field of Vector4</returns>
	float& operator[](int i);

	/// <summary>
	/// Addition operator of two Vector4
	/// </summary>
	/// <param name="addend">Addend of operation</param>
	/// <returns>New Vector4 object which is sum of this and addend</returns>
	Vector4 operator+(const Vector4& addend) const;

	/// <summary>
	/// Subtraction operator of two Vector4
	/// </summary>
	/// <param name="subtrahend">Subtrahend of operation</param>
	/// <returns>New Vector4 object which is difference of this and subtrahend</returns>
	Vector4 operator-(const Vector4& subtrahend) const;

	/// <summary>
	/// Vector4 to string
	/// </summary>
	/// <returns>String consisting of x, y, z, w values</returns>
	std::string toString();

	/// <summary>
	/// Dot product
	/// </summary>
	/// <param name="other">Second Vector4 used in the multiplication</param>
	/// <returns>Float value of dot product</returns>
	float dot(Vector4 other);

private:

	/// <summary>
	/// Float value of x
	/// </summary>
	float x;

	/// <summary>
	/// Float value of y
	/// </summary>
	float y;

	/// <summary>
	/// Float value of z
	/// </summary>
	float z;

	/// <summary>
	/// Float value of w
	/// </summary>
	float w;
};

/// <summary>
/// Overloaded operator for Vector4 class (aka "toString")
/// </summary>
/// <param name="out">Handle to ostream object</param>
/// <param name="vector">Vector4 which will be made to string</param>
/// <returns> Handle to output stream with string Vector4 data </returns>
std::ostream& operator<< (std::ostream& out, Vector3 vector);