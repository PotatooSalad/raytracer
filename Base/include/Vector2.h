#pragma once
#include <ostream>

class Vector3;

/// <summary>
/// Vector class with two coordinates: x and y
/// </summary>
class Vector2
{
public:
	/// <summary>
	/// Vector2 deafult constructor
	/// </summary>
	Vector2();

	/// <summary>
	/// Two-parameter constructor of Vector2
	/// </summary>
	/// <param name="x">Value of X coordinate</param>
	/// <param name="y">Value of Y coordinate</param>
	Vector2(float x, float y);

	/// <summary>
	/// Vector3 to Vector2 parsing constructor
	/// </summary>
	/// <param name="v">vector3 to parse</param>
	Vector2(Vector3 v);

	/// <summary>
	/// Constructor for Vector2 that makes all coordinates the same
	/// </summary>
	/// <param name="x">Value of coordinates</param>
	Vector2(float x);

	/// <summary>
	/// X coordinate getter
	/// </summary>
	/// <returns>Value of X coordinate</returns>
	float getX() { return x; }

	/// <summary>
	/// Y coordinate getter
	/// </summary>
	/// <returns>Value of Y coordinate</returns>
	float getY() { return y; }

	/// <summary>
	/// Setter of float value x
	/// </summary>
	/// <param name="newX">New value of x</param>
	void setX(float newX) { x = newX; }

	/// <summary>
	/// Setter of float value y
	/// </summary>
	/// <param name="newY">New value of y</param>
	void setY(float newY) { y = newY; }

	/// <summary>
	/// Getter of length of the Vector3
	/// </summary>
	/// <returns>Float value of length of the vector</returns>
	float getLength();

	/// <summary>
	/// Getter of square of length of the Vector3
	/// </summary>
	/// <returns>Float value of square of length of the vector</returns>
	float getLengthSquared();

	/// <summary>
	/// Normalization of the Vector2
	/// </summary>
	/// <returns>Return new Vector2 which is normalized from this</returns>
	Vector2 normalized();

	/// <summary>
	/// Comparison function for two Vector2
	/// </summary>
	/// <param name="other">Other Vector2 for comparison</param>
	/// <returns>True if all three coordinates are approximately equals (epsilon = 0.001).
	/// False, if they are not</returns>
	bool equals(Vector2 other);

	/// <summary>
	/// Addition operator of two Vectors
	/// </summary>
	/// <param name="addend">Addend of operation</param>
	/// <returns>New Vector3 object which is sum of this and addend</returns>
	Vector2 operator+(const Vector2& addend) const;

	/// <summary>
	/// Subtraction operator of two Vectors
	/// </summary>
	/// <param name="subtrahend">Subtrahend of operation</param>
	/// <returns>New Vector3 object which is difference of this and subtrahend</returns>
	Vector2 operator-(const Vector2& subtrahend) const;

	/// <summary>
	/// Multiplication operator of Vector2 and float
	/// </summary>
	/// <param name="factor">Factor of operation</param>
	/// <returns>New Vector3 object which is product of this and factor</returns>
	Vector2 operator*(const float& factor) const;

	/// <summary>
	/// Division operator of Vector2 and float
	/// </summary>
	/// <param name="divisor">Divisor of operation</param>
	/// <exception cref="std::exception">Thrown when divider is zero</exception>
	/// <returns>New Vector3 object which is quotient of this and divisor</returns>
	Vector2 operator/(const float& divisor) const;

	/// <summary>
	/// Addition with assignment operator of two Vectors
	/// </summary>
	/// <param name="addend">Addend of operation</param>
	/// <returns>Reference to this object after addition the addend to this</returns>
	Vector2& operator+=(const Vector2& addend);

	/// <summary>
	/// Subtraction with assignment operator of two Vectors
	/// </summary>
	/// <param name="subtrahend">Subtrahend of operation</param>
	/// <returns>Reference to this object after subtraction the subtrahend from this</returns>
	Vector2& operator-=(const Vector2& subtrahend);

	/// <summary>
	/// Multiplication with assignment operator of Vector2 and float
	/// </summary>
	/// <param name="factor">Factor of operation</param>
	/// <returns>Reference to this object after multiplication the factor to this</returns>
	Vector2& operator*=(const float& factor);

	/// <summary>
	/// Division with assignment operator of Vector2 and float
	/// </summary>
	/// <param name="divisor">Divisor of operation</param>
	/// <returns>Reference to this object after division of this by divisor</returns>
	/// <exception cref="std::exception">Thrown when divider is zero</exception>
	Vector2& operator/=(const float& divisor);

	/// <summary>
	/// Left-side negation operator
	/// </summary>
	/// <returns>New Vector3 object with negated coordinates from this</returns>
	Vector2 operator-() const;

	/// <summary>
	/// Vector3 initialized with zeros
	/// </summary>
	static const Vector2 zero;

	/// <summary>
	/// Vector3 initialized with ones
	/// </summary>
	static const Vector2 one;

	/// <summary>
	/// Checks if vector is all zeros
	/// </summary>
	/// <returns>True if vector is zero, false - if it's not</returns>
	bool isZero();

private:
	/// <summary>
	/// X coordinate of Vector2
	/// </summary>
	float x;

	/// <summary>
	/// Y coordinate of Vector2
	/// </summary>
	float y;
};

/// <summary>
/// Multiplication operator of float and Vector2
/// </summary>
/// <param name="factor">Float factor of operation</param>
/// <param name="vfactor">Vector2 factor of operation</param>
/// <returns>New Vector2 object which is product of factor and vfactor</returns>
static Vector2 operator*(const float& factor, const Vector2 vfactor)
{
	return vfactor * factor;
}

/// <summary>
/// Overloaded operator for Vector2 class (aka "toString")
/// </summary>
/// <param name="out">Handle to ostream object</param>
/// <param name="vector">Vector2 which will be made to string</param>
/// <returns> Handle to output stream with string Vector2 data </returns>
std::ostream& operator<< (std::ostream& out, Vector2 vector);
