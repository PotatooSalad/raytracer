#pragma once

using byte = unsigned char;
using uint = unsigned int;

/// <summary>
/// Checks if float is zero
/// </summary>
/// <param name="f">Float to check</param>
/// <returns>If value of float is zero</returns>
bool isFloatZero(float f);

/// <summary>
/// Checks if float is a number, without zero
/// </summary>
/// <param name="f">Float to check</param>
/// <returns>If value of float is number but not 0</returns>
bool isFloatNormal(float f);