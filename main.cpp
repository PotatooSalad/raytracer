#include <iostream>
#include <iomanip>

#include "Sphere.h"
#include "Image.h"
#include "LightIntensity.h"
#include "PixelColor.h"
#include "OrthogonalCamera.h"
#include "PerspectiveCamera.h"
#include "Scene.h"
#include "Material.h"
#include "ObjParser.h"
#include "Matrix4x4.h"
#include "PointLight.h"
#include "DirectionalLight.h"
#include "SpotLight.h"
#include "Texture.h"
#include "Mesh.h"
#include "Plane.h"
#include <chrono>

int main()
{
	auto applicationStart = std::chrono::system_clock::now();
	const int width = 512;
	const int height = width;

	Texture texture = Texture("tex.png", 3);
	Texture zawarudo = Texture("merTex.png", 3);
	Scene scene;

	//orange
	PointLight pl = PointLight(
		Vector3(0, 1, 3),
		LightRange::dist32,
		LightIntensity(0.2, 0.2, 0.2),
		LightIntensity(1, 1, 1),
		LightIntensity(0.949, 0.949, 0.949)
	);

	DirectionalLight dl = DirectionalLight(
		Vector3(-1, -1, 0).normalized(),
		LightIntensity(0.231, 0.231, 0.231),
		LightIntensity(0.870, 0.870, 0.870),
		LightIntensity(1.0, 1.0, 1.0)
	);

	SpotLight sl = SpotLight(
		Vector3(0, 0, 0), Vector3(0, 0, 1).normalized(),
		40, 70, LightRange::dist32,
		LightIntensity(0.231, 0.231, 0.231),
		LightIntensity(0.870, 0.870, 0.870),
		LightIntensity(1.0, 1.0, 1.0)
	);

	//scene.setDirectional(&dl);
	scene.addOtherLight(&pl);
	//scene.addOtherLight(&sl);

	Sphere s1(Vector3(-1, -1.75, 3), 0.75f);
	//violet
	Material m1(
		"",
		LightIntensity(0.580, 0.278, 0.882),
		0.8, 8.0, MaterialType::GLASS, 1.52
	);
	//m1.setTexture(&zawarudo);
	scene.makeAndGetNewSceneObject(&s1, &m1);

	Sphere s2(Vector3(1, -1.75, 3), 0.75f);
	//red
	Material m2(
		"",
		LightIntensity(0.858, 0.141, 0.215),
		0.0, 1.0, MaterialType::MIRROR
	);
	scene.makeAndGetNewSceneObject(&s2, &m2);

	Material green = Material("",
		LightIntensity(0.133, 0.643, 0.039),
		0.5f);

	Material gray = Material("",
		LightIntensity(0.678, 0.678, 0.678),
		0.5f);

	Material purple = Material("",
		LightIntensity(0.650, 0, 0.721),
		0.5f);

	Material blue = Material("",
		LightIntensity(0.058, 0.098, 0.419),
		0.5f);

	Plane p1(Vector3(0, 1, 0), Vector3(0, -3, 0));
	scene.makeAndGetNewSceneObject(&p1, &green);

	Plane p2(Vector3(-1, 0, 0), Vector3(3, 0, 0));
	scene.makeAndGetNewSceneObject(&p2, &purple);

	Plane p3(Vector3(0, -1, 0), Vector3(0, 3, 0));
	scene.makeAndGetNewSceneObject(&p3, &blue);

	Plane p4(Vector3(1, 0, 0), Vector3(-3, 0, 0));
	scene.makeAndGetNewSceneObject(&p4, &purple);

	Plane p5(Vector3(0, 0, -1), Vector3(0, 0, 5));
	scene.makeAndGetNewSceneObject(&p5, &gray);

	Plane p6(Vector3(0, 0, 1), Vector3(0, 0, -1));
	scene.makeAndGetNewSceneObject(&p6, &gray);

	Sphere s3 = Sphere(pl.getLightPosition(), 0.05f);
	Material m3("", LightIntensity(1, 0, 1), 0.0, 0.0, MaterialType::NONE);
	scene.makeAndGetNewSceneObject(&s3, &m3);

	//ObjParser parser = ObjParser();
	//Matrix4x4 temp = Matrix4x4::identity;
	//temp = temp.translate(Vector3(0.0f, -2.0f, 5.0f));
	////temp = temp.rotate(Vector3(0, 1, 0), 180);
	////temp = temp.scale(Vector3(1.5f));
	//std::vector<Mesh*> meshes;
	//std::vector<Material*> materials;
	//parser.parseToMeshesWithMaterials("cornell.obj", meshes, materials, temp);

	//for (auto m : meshes)
	//{
	//	if (m->getMaterial()->getName().compare("Purple") == 0)
	//	{
	//		m->getMaterial()->setTexture(&texture);
	//	}
	//	scene.addMesh(m);
	//}

	Image bufferImage(width, height);
	bufferImage.clearColor(PixelColor(0));

	PerspectiveCamera perspCamera = PerspectiveCamera(Vector3::zero, Vector3(0.0f, 0.0f, 1.0f), 90.0f);
	perspCamera.assignScene(&scene);

	auto start = std::chrono::system_clock::now();
	perspCamera.renderScene(&bufferImage, Antyaliasing::REGULAR);
	auto end = std::chrono::system_clock::now();
	std::chrono::duration<double> duration = end - start;
	std::cout << "Regular: " << duration.count() << std::endl;
	bufferImage.saveToPng("Regular.png");
	bufferImage.clearColor(PixelColor(0));

	start = std::chrono::system_clock::now();
	perspCamera.renderScene(&bufferImage, Antyaliasing::RANDOM);
	end = std::chrono::system_clock::now();
	duration = end - start;
	std::cout << "Random: " << duration.count() << std::endl;
	bufferImage.saveToPng("Random.png");
	bufferImage.clearColor(PixelColor(0));

	start = std::chrono::system_clock::now();
	perspCamera.renderScene(&bufferImage, Antyaliasing::JITTER);
	end = std::chrono::system_clock::now();
	duration = end - start;
	std::cout << "Jitter: " << duration.count() << std::endl;
	bufferImage.saveToPng("Jitter.png");
	bufferImage.clearColor(PixelColor(0));

	start = std::chrono::system_clock::now();
	perspCamera.renderScene(&bufferImage, Antyaliasing::DISK);
	end = std::chrono::system_clock::now();
	duration = end - start;
	std::cout << "Poisson: " << duration.count() << std::endl;
	bufferImage.saveToPng("Poisson.png");
	bufferImage.clearColor(PixelColor(0));

	start = std::chrono::system_clock::now();
	perspCamera.renderScene(&bufferImage, Antyaliasing::ADAPTIVE);
	end = std::chrono::system_clock::now();
	duration = end - start;
	std::cout << "Adaptive: " << duration.count() << std::endl << std::endl;
	bufferImage.saveToPng("Adaptive.png");
	bufferImage.clearColor(PixelColor(0));

	start = std::chrono::system_clock::now();
	perspCamera.renderScene(&bufferImage, Antyaliasing::NONE);
	end = std::chrono::system_clock::now();
	duration = end - start;
	std::cout << "None: " << duration.count() << std::endl << std::endl;
	bufferImage.saveToPng("None.png");

	duration = end - applicationStart;
	std::cout << "Whole program duration: " << duration.count() << std::endl;

	/*OrthogonalCamera orthoCamera = OrthogonalCamera(Vector3::zero, Vector3(0.0f, 0.0f, 1.0f));
	orthoCamera.assignScene(&scene);
	orthoCamera.renderScene(&bufferImage);
	bufferImage.saveToTga("orthogonalImage.tga");

	std::cout << "Orthogonal Camera Image saved!\n";*/

	return 0;
}