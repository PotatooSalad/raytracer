#pragma once
#include "Hitable.h"
#include "Vertex.h"

/// <summary>
/// Triangle object class
/// </summary>
class Triangle : public Hitable
{
public:
	/// <summary>
	/// Triangle constructor
	/// </summary>
	/// <param name="v1">First vertex of triangle</param>
	/// <param name="v2">Second vertex of triangle</param>
	/// <param name="v3">Third vertex of triangle</param>
	/// <param name="objectId">Id of mesh object</param>
	Triangle(Vertex v1, Vertex v2, Vertex v3, int objectId = 0);

	/// <summary>
	/// Counting intersection between triangle and ray
	/// </summary>
	/// <param name="ray">Ray object which with object intersects (or not)</param>
	/// <param name="maxDistance">>Max detection distance from ray origin</param>
	/// <param name="light">If intersection is counted for light</param>
	/// <returns>Closest distance to ray origin; If ray doesn't intersect, returns maxDistance</returns>
	float countIntersection(Ray& ray, float maxDistance, bool light);


	/// <summary>
	/// Return type name (for debug purposes)
	/// </summary>
	/// <returns>Class name</returns>
	const char* type() { return "Triangle"; }

	/// <summary>
	/// Get normal vector from triangle in specific point (if that point is on triangle)
	/// </summary>
	/// <param name="point">Point from which will be counted normal vector</param>
	/// <returns>Normal vector in that point</returns>
	Vector3 normalInPoint(Vector3 point);

	/// <summary>
	/// Calculate UV coordinates from hitable in specific point
	/// </summary>
	/// <param name="intersectionPoint">Point on the surface of the Hitable object</param>
	/// <returns>UV coordinates of hitable</returns>
	Vector2 calculateUVCoordinates(Vector3 intersectionPoint);

	/// <summary>
	/// Ostream operator overload
	/// </summary>
	/// <param name="out">Handle to ostream</param>
	/// <param name="t">Triangle to parse</param>
	/// <returns>Handle to ostream which includes parsed triangle</returns>
	friend std::ostream& operator<<(std::ostream& out, Triangle& t);

private:
	Vertex v1;
	Vertex v2;
	Vertex v3;

	/// <summary>
	/// Counts triangle barycentric in point
	/// </summary>
	/// <param name="point">Point to calculate barycentric</param>
	/// <returns>Triangle barycentric in point</returns>
	Vector3 getTriangleBarycentric(Vector3 point);

	
};
