#pragma once

#include "Hitable.h"
#include "Vector3.h"
#include "Vector2.h"

/// <summary>
/// Implementation of Plane with general and vector equations
/// </summary>
class Plane : public Hitable
{
public:
	/// <summary>
	/// Default constructor of plane. Creates plane with directional vector (0, 1, 0) on XZ axes
	/// </summary>
	Plane();

	/// <summary>
	/// Constuctor of plane using general equation.
	/// </summary>
	/// <param name="a, b, c, d">General equation of plane factors</param>
	Plane(float a, float b, float c, float d);

	/// <summary>
	/// Constructor of plane using normal vector of plane and one of plane points
	/// </summary>
	/// <param name="normalVector"></param>
	/// <param name="planePoint"></param>
	Plane(Vector3 normalVector, Vector3 planePoint);

	/// <summary>
	/// Counting intersection between plane and ray
	/// </summary>
	/// <param name="ray">Ray object which with object intersects (or not)</param>
	/// <param name="maxDistance">>Max detection distance from ray origin</param>
	/// <param name="light">If intersection is counted for light</param>
	/// <returns>Closest distance to ray origin; If ray doesn't intersect, returns maxDistance</returns>
	float countIntersection(Ray& ray, float maxDistance, bool light);

	/// <summary>
	/// Return type name (for debug purposes)
	/// </summary>
	/// <returns>Class name</returns>
	const char* type() { return "Plane"; }

	/// <summary>
	/// Get normal vector from plane in specific point (if that point is on plane)
	/// </summary>
	/// <param name="point">Point from which will be counted normal vector</param>
	/// <returns>Normal vector in that point</returns>
	Vector3 normalInPoint(Vector3 point);

	/// <summary>
	/// Count AABB for plane - which means, do nothing
	/// </summary>
	void countAABB() {};

	/// <summary>
	/// Calculate UV coordinates from plane in specific point
	/// </summary>
	/// <param name="intersectionPoint">Point on the surface of the Plane object</param>
	/// <returns>UV coordinates of Plane</returns>
	Vector2 calculateUVCoordinates(Vector3 intersectionPoint) { return Vector2::zero; };

private:
	/// <summary>
	/// Normal vector of plane
	/// </summary>
	Vector3 normalVector;
	/// <summary>
	/// Point that belongs to plane
	/// </summary>
	Vector3 planePoint;
	/// <summary>
	/// Shift the plane along the normalVector from (0,0,0): -D
	/// </summary>
	float d;
};

