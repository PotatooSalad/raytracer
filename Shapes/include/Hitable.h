#pragma once
#include "Intersection.h"

class Ray;
class Vector3;
class Vector2;

/// <summary>
/// Abstract class that marks objects which can be hit by a ray
/// </summary>
class Hitable
{
public:
	/// <summary>
	/// Counting intersection between object and ray
	/// </summary>
	/// <param name="ray">Ray object which with object intersects (or not)</param>
	/// <param name="maxDistance">>Max detection distance from ray origin</param>
	/// <param name="light">If intersection is counted for light</param>
	/// <returns>Closest distance to ray origin; If ray doesn't intersect, returns maxDistance</returns>
	virtual float countIntersection(Ray& ray, float maxDistance, bool light) = 0;

	/// <summary>
	/// Return type name (for debug purposes)
	/// </summary>
	/// <returns>Class name</returns>
	virtual const char* type() = 0;

	/// <summary>
	/// Get normal vector from hitable in specific point (if that point is on hitable object)
	/// </summary>
	/// <param name="point">Point from which will be counted normal vector</param>
	/// <returns>Normal vector in that point</returns>
	virtual Vector3 normalInPoint(Vector3 point) = 0;

	/// <summary>
	/// Calculate UV coordinates from hitable in specific point
	/// </summary>
	/// <param name="intersectionPoint">Point on the surface of the Hitable object</param>
	/// <returns>UV coordinates of hitable</returns>
	virtual Vector2 calculateUVCoordinates(Vector3 intersectionPoint) = 0;

};