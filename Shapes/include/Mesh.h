#pragma once

#include "globals.h"
#include "Triangle.h"
#include "Matrix4x4.h"

#include <vector>
class Material;
class SceneObject;

/// <summary>
/// Mesh class - temporary object between obj file and scene
/// </summary>
class Mesh
{
public:
	/// <summary>
	/// Constructor of Mesh object
	/// </summary>
	/// <param name="vertices">Vector of vertices of mesh</param>
	/// <param name="indices">Vector of indices of mesh (may be size 0)</param>
	/// <param name="mat"> Mesh material </param>
	/// <param name="objToWorld">Object to world Matrix</param>
	Mesh(std::vector<Vertex>& vertices, std::vector<uint>& indices, Matrix4x4 objToWorld, Material* mat);

	/// <summary>
	/// Destructor of Mesh object
	/// </summary>
	~Mesh();

	/// <summary>
	/// Return type name (for debug purposes)
	/// </summary>
	/// <returns>Class name</returns>
	const char* type() { return "Mesh"; }

	/// <summary>
	/// OBB test for mesh
	/// </summary>
	/// <param name="ray">Refrence to ray object</param>
	/// <param name="maxDistance">Maximum distance to test</param>
	/// <returns>True if ray intersects obb</returns>
	bool testOBB(Ray& ray, float maxDistance);

	/// <summary>
	/// Get mesh material
	/// </summary>
	/// <returns>Pointer to material object</returns>
	Material* getMaterial() { return material; }

	/// <summary>
	/// Get mesh triangles
	/// </summary>
	/// <returns>Mesh triangles</returns>
	std::vector<SceneObject*>* getTriangles() { return &sceneTriangles; }

private:
	/// <summary>
	/// Method to find maximum value from 4 components
	/// </summary>
	/// <param name="a">First component</param>
	/// <param name="b">Second component</param>
	/// <param name="c">Third component</param>
	/// <param name="d">Fourth component</param>
	/// <returns>Maximum value of all</returns>
	float max(float a, float b, float c, float d);

	/// <summary>
	/// Method to find minimum value from 4 components
	/// </summary>
	/// <param name="a">First component</param>
	/// <param name="b">Second component</param>
	/// <param name="c">Third component</param>
	/// <param name="d">Fourth component</param>
	/// <returns>Minimum value of all</returns>
	float min(float a, float b, float c, float d);

	/// <summary>
	/// Vector of mesh triangles
	/// </summary>
	std::vector<Triangle*> triangles;

	/// <summary>
	/// 
	/// </summary>
	std::vector<SceneObject*> sceneTriangles;

	/// <summary>
	/// Mesh material
	/// </summary>
	Material* material = nullptr;

	/// <summary>
	/// Matrix object to world
	/// </summary>
	Matrix4x4 objToWorldMatrix;

	/// <summary>
	/// Bounding box minimum
	/// </summary>
	Vector3 aabbMin;

	/// <summary>
	/// Bounding box maximum
	/// </summary>
	Vector3 aabbMax;
};

