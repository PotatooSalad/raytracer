#pragma once
#include "Hitable.h"
#include "Vector3.h"

/// <summary>
/// Implementation of Sphere
/// </summary>
class Sphere : public Hitable
{
public:
	/// <summary>
	/// Sphere class constructor
	/// </summary>
	/// <param name="center">Center point of sphere</param>
	/// <param name="radius">Radius of sphere</param>
	Sphere(Vector3 center, float radius);

	/// <summary>
	/// Default constuctor of sphere with center in (0,0,0) and radius 1
	/// </summary>
	Sphere();

	/// <summary>
	/// Counting intersection between sphere and ray
	/// </summary>
	/// <param name="ray">Ray object which with object intersects (or not)</param>
	/// <param name="maxDistance">>Max detection distance from ray origin</param>
	/// <param name="light">If intersection is counted for light</param>
	/// <returns>Closest distance to ray origin; If ray doesn't intersect, returns maxDistance</returns>
	float countIntersection(Ray& ray, float maxDistance, bool light);

	/// <summary>
	/// Return type name (for debug purposes)
	/// </summary>
	/// <returns>Class name</returns>
	const char* type() { return "Sphere"; }

	/// <summary>
	/// Get normal vector from sphere in specific point (if that point is on sphere)
	/// </summary>
	/// <param name="point">Point from which will be counted normal vector</param>
	/// <returns>Normal vector in that point</returns>
	Vector3 normalInPoint(Vector3 point);

	/// <summary>
	/// Calculate UV coordinates from Sphere in specific point
	/// </summary>
	/// <param name="intersectionPoint">Point on the surface of the Sphere object</param>
	/// <returns>UV coordinates of sphere</returns>
	Vector2 calculateUVCoordinates(Vector3 intersectionPoint);

private:
	/// <summary>
	/// Center point of sphere
	/// </summary>
	Vector3 center;
	/// <summary>
	/// Radius of sphere
	/// </summary>
	float radius;
	/// <summary>
	/// Squared value of radius (optimization purposes)
	/// </summary>
	float squareRadius;
};

