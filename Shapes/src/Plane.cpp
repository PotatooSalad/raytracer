#include "Plane.h"
#include "globals.h"
#include "Vector3.h"
#include "Intersection.h"
#include "Ray.h"

Plane::Plane()
{
	normalVector = Vector3(0, 1, 0);
	planePoint = Vector3::zero;
	this->d = 0.0f;
}

Plane::Plane(float a, float b, float c, float d)
{
	Vector3 temp(a, b, c);
	if (temp.isZero())
	{
		throw std::exception("Plane normal vector cannot be zero!");
	}
	normalVector = temp.normalized();
	if (isFloatZero(c))
	{
		planePoint = Vector3(0.0f, 0.0f, -d / c);
	}
	else if (isFloatZero(b))
	{
		planePoint = Vector3(0.0f, -d / b, 0.0f);
	}
	else
	{
		planePoint = Vector3(-d / a, 0.0f, 0.0f);
	}
	this->d = -d;
}

Plane::Plane(Vector3 normalVector, Vector3 planePoint)
{
	if (normalVector.isZero())
	{
		throw std::exception("Plane normal vector cannot be zero!");
	}
	this->normalVector = normalVector.normalized();
	this->planePoint = planePoint;
	this->d = normalVector.dot(planePoint);
}

float Plane::countIntersection(Ray& ray, float maxDistance, bool light)
{
	float nDotV = normalVector.dot(ray.getDirection());
	float returnValue = maxDistance + 1.0f;
	if (isFloatNormal(nDotV))
	{
		float t = (d - normalVector.dot(ray.getOrigin())) / nDotV;
		if (t >= 0.001 && maxDistance > t)
		{
			returnValue = t;
		}
	}
	return returnValue;
}

Vector3 Plane::normalInPoint(Vector3 point)
{
	return normalVector;
}