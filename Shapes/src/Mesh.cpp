#include "Mesh.h"
#include "Matrix4x4.h"
#include "Ray.h"
#include "SceneObject.h"

Mesh::Mesh(std::vector<Vertex>& vertices, std::vector<uint>& indices, Matrix4x4 objToWorld, Material* mat)
{
	this->objToWorldMatrix = objToWorld;
	this->material = mat;
	aabbMin = Vector3(INFINITY);
	aabbMax = Vector3::zero;
	if (indices.size() > 0)
	{
		for (int i = 0; i < indices.size(); i += 3)
		{
			Vertex v1 = vertices[indices[i]];
			Vertex v2 = vertices[indices[i + 1]];
			Vertex v3 = vertices[indices[i + 2]];
			Vector4 tempV1 = objToWorld * Vector4(v1.position, 1.0f);
			Vector4 tempV2 = objToWorld * Vector4(v2.position, 1.0f);
			Vector4 tempV3 = objToWorld * Vector4(v3.position, 1.0f);
			v1.position = Vector3(tempV1);
			v2.position = Vector3(tempV2);
			v3.position = Vector3(tempV3);

			v1.normal = Vector3(objToWorld * Vector4(v1.normal, 0.0f)).normalized();
			v2.normal = Vector3(objToWorld * Vector4(v2.normal, 0.0f)).normalized();
			v3.normal = Vector3(objToWorld * Vector4(v3.normal, 0.0f)).normalized();

			aabbMax.setX(max(aabbMax.getX(), v1.position.getX(), v2.position.getX(), v3.position.getX()));
			aabbMax.setY(max(aabbMax.getY(), v1.position.getY(), v2.position.getY(), v3.position.getY()));
			aabbMax.setZ(max(aabbMax.getZ(), v1.position.getZ(), v2.position.getZ(), v3.position.getZ()));

			aabbMin.setX(min(aabbMin.getX(), v1.position.getX(), v2.position.getX(), v3.position.getX()));
			aabbMin.setY(min(aabbMin.getY(), v1.position.getY(), v2.position.getY(), v3.position.getY()));
			aabbMin.setZ(min(aabbMin.getZ(), v1.position.getZ(), v2.position.getZ(), v3.position.getZ()));

			triangles.push_back(new Triangle(v1, v2, v3));
		}
	}
	else
	{
		for (int i = 0; i < vertices.size(); i += 3)
		{
			Vertex v1 = vertices[i];
			Vertex v2 = vertices[i + 1];
			Vertex v3 = vertices[i + 2];
			Vector4 tempV1 = objToWorld * Vector4(v1.position, 1.0f);
			Vector4 tempV2 = objToWorld * Vector4(v2.position, 1.0f);
			Vector4 tempV3 = objToWorld * Vector4(v3.position, 1.0f);
			v1.position = Vector3(tempV1);
			v2.position = Vector3(tempV2);
			v3.position = Vector3(tempV3);

			/*v1.normal = Vector3(objToWorld * Vector4(v1.normal, 0.0f)).normalized();
			v2.normal = Vector3(objToWorld * Vector4(v2.normal, 0.0f)).normalized();
			v3.normal = Vector3(objToWorld * Vector4(v3.normal, 0.0f)).normalized();*/

			aabbMax.setX(max(aabbMax.getX(), v1.position.getX(), v2.position.getX(), v3.position.getX()));
			aabbMax.setY(max(aabbMax.getY(), v1.position.getY(), v2.position.getY(), v3.position.getY()));
			aabbMax.setZ(max(aabbMax.getZ(), v1.position.getZ(), v2.position.getZ(), v3.position.getZ()));

			aabbMin.setX(min(aabbMin.getX(), v1.position.getX(), v2.position.getX(), v3.position.getX()));
			aabbMin.setY(min(aabbMin.getY(), v1.position.getY(), v2.position.getY(), v3.position.getY()));
			aabbMin.setZ(min(aabbMin.getZ(), v1.position.getZ(), v2.position.getZ(), v3.position.getZ()));

			triangles.push_back(new Triangle(v1, v2, v3));
		}
	}
	for (auto* t : triangles)
	{
		sceneTriangles.push_back(new SceneObject(t, material));
	}
}

Mesh::~Mesh()
{
	for (int i = 0; i < triangles.size(); ++i)
	{
		delete triangles[i];
		triangles[i] = nullptr;
	}
	triangles.clear();
}

float Mesh::max(float a, float b, float c, float d)
{
	return std::max(a, std::max(b, std::max(c, d)));
}

float Mesh::min(float a, float b, float c, float d)
{
	return std::min(a, std::min(b, std::min(c, d)));
}

bool Mesh::testOBB(Ray& ray, float maxDistance)
{
	// ray world to mesh space
	//Matrix4x4 worldToObj = objToWorldMatrix.
	return true;
}