#include "Sphere.h"
#include "Vector3.h"
#include "Vector2.h"
#include "Intersection.h"
#include "Ray.h"
#define _USE_MATH_DEFINES
#include <math.h>

Sphere::Sphere(Vector3 center, float radius)
{
	this->center = center;
	this->radius = radius;
	squareRadius = radius * radius;
}

Sphere::Sphere()
{
	this->center = Vector3::zero;
	this->radius = 1.0f;
	squareRadius = 1.0f;
}

float Sphere::countIntersection(Ray& ray, float maxDistance, bool light)
{
	Vector3 vec = ray.getOrigin() - center;
	Vector3 rayDirection = ray.getDirection();
	float a = rayDirection.dot(rayDirection);
	float b = rayDirection.dot(vec);
	float c = vec.dot(vec) - squareRadius;
	float det = (b * b) - a * c;
	float returnValue = maxDistance + 1.0f;
	if (det > 0.0f)
	{
		det = sqrtf(det);
		float i1 = (-b - det) / a;
		float i2 = (-b + det) / a;
		if (i2 > 0)
		{
			if (i1 < 0 && i2 < maxDistance)
			{
				returnValue = i2;
			}
			else if (i1 < maxDistance)
			{
				returnValue = i1;
			}
		}
	}
	else if (isFloatZero(det))
	{
		float i0 = -b / a;
		if (i0 < maxDistance)
		{
			returnValue = i0;
		}
	}
	return returnValue;
}

Vector3 Sphere::normalInPoint(Vector3 point)
{
	return (point - center).normalized();
}

Vector2 Sphere::calculateUVCoordinates(Vector3 intersectionPoint)
{
	Vector3 localHitPoint = intersectionPoint - center;
	localHitPoint.normalize();
	float theta = acosf(localHitPoint.getY());
	float phi = atan2(localHitPoint.getX(), localHitPoint.getZ());
	if (phi < 0.0f)
		phi += M_PI + M_PI;

	float invTWO_PI = 1 / (M_PI + M_PI);
	Vector2 uv;
	uv.setX(1.0f - phi * invTWO_PI);
	uv.setY(theta * M_1_PI);
	return uv;
}