#include "Triangle.h"
#include "Plane.h"
#include "Intersection.h"
#include "Ray.h"

Triangle::Triangle(Vertex v1, Vertex v2, Vertex v3, int objectId)
{
	this->v1 = v1;
	this->v2 = v2;
	this->v3 = v3;
}

float Triangle::countIntersection(Ray& ray, float maxDistance, bool light)
{
	const float EPSILON = 0.00001f;

	Vector3 edge1 = v2.position - v1.position;
	Vector3 edge2 = v3.position - v1.position;

	Vector3 h = ray.getDirection().cross(edge2);
	float a = edge1.dot(h);

	if (a > -EPSILON && a < EPSILON)
	{
		return maxDistance + 1.0f;
	}

	float f = 1.0f / a;
	Vector3 s = ray.getOrigin() - v1.position;
	float u = f * s.dot(h);

	if (u < 0.0f || u > 1.0f)
	{
		return maxDistance + 1.0f;
	}

	Vector3 q = s.cross(edge1);
	float v = f * ray.getDirection().dot(q);

	if (v < 0.0f || u + v > 1.0f)
	{
		return maxDistance + 1.0f;
	}

	float t = f * edge2.dot(q);
	if (t > EPSILON)
	{
		return t;
	}
	else
	{
		return maxDistance + 1.0f;
	}	
}

Vector3 Triangle::normalInPoint(Vector3 point)
{
	Vector3 aNorm = v1.normal.normalized();
	Vector3 bNorm = v2.normal.normalized();
	Vector3 cNorm = v3.normal.normalized();

	Vector3 barycentric = getTriangleBarycentric(point);
	return (barycentric.getX() * aNorm + barycentric.getY() * bNorm + barycentric.getZ() * cNorm).normalized();
}

Vector2 Triangle::calculateUVCoordinates(Vector3 intersectionPoint)
{
	Vector2 aUV = v1.texCoord;
	Vector2 bUV = v2.texCoord;
	Vector2 cUV = v3.texCoord;

	Vector3 barycentric = getTriangleBarycentric(intersectionPoint);

	return barycentric.getX() * aUV + barycentric.getY() * bUV + barycentric.getZ() * cUV;
}

Vector3 Triangle::getTriangleBarycentric(Vector3 point)
{
	Vector3 a = v1.position;
	Vector3 b = v2.position;
	Vector3 c = v3.position;

	Vector3 v0 = b - a;
	Vector3 v1 = c - a;
	Vector3 v2 = point - a;
	float d00 = v0.dot(v0);
	float d01 = v0.dot(v1);
	float d11 = v1.dot(v1);
	float d20 = v2.dot(v0);
	float d21 = v2.dot(v1);
	float denom = 1.0 / ((double)d00 * d11 - (double)d01 * d01);
	
	float y = (d11 * d20 - d01 * d21) * denom;
	float z = (d00 * d21 - d01 * d20) * denom;
	float x = 1.0f - y - z;
	return Vector3(x, y, z);
}

std::ostream& operator<<(std::ostream& out, Triangle& t)
{
	out << "v1: " << t.v1 << "\nv2: " << t.v2 << "\nv3: " << t.v3 << std::endl;
	return out;
 }