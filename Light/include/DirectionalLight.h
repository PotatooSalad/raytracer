#pragma once
#include "Vector3.h"
#include "Light.h"

/// <summary>
/// Directional Light Class
/// </summary>
class DirectionalLight : public Light
{
public:
	/// <summary>
	/// Directional light class constructor
	/// </summary>
	/// <param name="direction">Direction of light</param>
	/// <param name="ambient">Ambient color of light</param>
	/// <param name="diffuse">Diffuse color of light</param>
	/// <param name="specular">Specular color of light</param>
	DirectionalLight(Vector3 direction, LightIntensity ambient, LightIntensity diffuse, LightIntensity specular) 
		: Light(ambient, diffuse, specular, Vector3::zero), direction(direction) {}

	/// <summary>
	/// Calculate directional light for scene object
	/// </summary>
	/// <param name="object">Scene object to calculate light</param>
	/// <param name="intersectionPoint">Intersection point of ray and object</param>
	/// <param name="cameraDirection">Direction vector of camera</param>
	/// <param name="shaded">Is object shaded (hidden by other)</param>
	/// <returns>Color of pixel after counting light properties</returns>
	LightIntensity calculateLight(SceneObject* object, Vector3 intersectionPoint, Vector3 cameraDirection, bool shaded);

	/// <summary>
	/// Directional light max distance
	/// </summary>
	/// <returns>Infinity</returns>
	float getLightMaxDistance() { return INFINITY; }

	/// <summary>
	/// Get directional light direction vector
	/// </summary>
	/// <param name="point">some point (it's not used, but it is abstract implementation)</param>
	/// <returns>Direction Vector</returns>
	Vector3 getLightDirection(Vector3 point) { return direction; }
private:
	/// <summary>
	/// Direction vector of light
	/// </summary>
	Vector3 direction;
};