#pragma once
#include "LightIntensity.h"
#include "PixelColor.h"
#include "Vector3.h"

class Scene;
class SceneObject;
class Ray;

/// <summary>
/// Light base class
/// </summary>
class Light
{
public:
	/// <summary>
	/// Basic light constructor
	/// </summary>
	/// <param name="ambient">Ambient value of light</param>
	/// <param name="diffuse">Diffuse value of light</param>
	/// <param name="specular">Specular value of light</param>
	Light(LightIntensity ambient, LightIntensity diffuse, LightIntensity specular, Vector3 position) 
		: ambient(ambient), diffuse(diffuse), specular(specular), position(position) {}

	/// <summary>
	/// Calculate light for scene object
	/// </summary>
	/// <param name="object">Scene object to calculate light</param>
	/// <param name="intersectionPoint">Intersection point of ray and object</param>
	/// <param name="cameraDirection">Direction vector of camera</param>
	/// <param name="shaded">Is object shaded (hidden by other)</param>
	/// <returns>Color of pixel after counting light properties</returns>
	virtual LightIntensity calculateLight(SceneObject* object, Vector3 intersectionPoint, Vector3 cameraDirection, bool shaded) = 0;

	/// <summary>
	/// Returns maximum distance for light to search objects
	/// </summary>
	/// <returns>Maximum distance for ray intersection</returns>
	virtual float getLightMaxDistance() = 0;

	/// <summary>
	/// Get light direction (from light to object)
	/// </summary>
	/// <param name="point">One of points of direction vector (if pointlight)</param>
	/// <returns>Direction of light</returns>
	virtual Vector3 getLightDirection(Vector3 point) = 0;

	/// <summary>
	/// Light Position getter
	/// </summary>
	/// <returns></returns>
	Vector3 getLightPosition() { return position; }
protected:
	/// <summary>
	/// Ambient value of light
	/// </summary>
	LightIntensity ambient;
	/// <summary>
	/// Diffuse value of light
	/// </summary>
	LightIntensity diffuse;
	/// <summary>
	/// Specular value of light
	/// </summary>
	LightIntensity specular;

	/// <summary>
	/// Position of light
	/// </summary>
	Vector3 position;
};