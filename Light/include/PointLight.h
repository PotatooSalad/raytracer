#pragma once
#include "Vector3.h"
#include "Light.h"
#include "Vector4.h"

/// <summary>
/// Namespace for point light light ranges attenuation values
/// </summary>
namespace LightRange
{
	const Vector4 dist7 = Vector4(1.0, 0.7, 1.8, 7);
	const Vector4 dist13 = Vector4(1.0, 0.35, 0.44, 13);
	const Vector4 dist20 = Vector4(1.0, 0.22, 0.20, 20);
	const Vector4 dist32 = Vector4(1.0, 0.14, 0.07, 32);
	const Vector4 dist50 = Vector4(1.0, 0.09, 0.032, 50);
	const Vector4 dist65 = Vector4(1.0, 0.07, 0.017, 65);
	const Vector4 dist100 = Vector4(1.0, 0.045, 0.0075, 100);
};

/// <summary>
/// point light class
/// </summary>
class PointLight : public Light
{
public:
	/// <summary>
	/// Point Light class constructor
	/// </summary>
	/// <param name="position">Position of light</param>
	/// <param name="attenuation">PointLight attenuation: x- constant, y - linear, z - quadratic, w - maxDistance</param>
	/// <param name="ambient">Ambient color of light</param>
	/// <param name="diffuse">Diffuse color of light</param>
	/// <param name="specular">Specular color of light</param>
	PointLight(Vector3 position, Vector4 attenuation, LightIntensity ambient, LightIntensity diffuse, LightIntensity specular)
		: Light(ambient, diffuse, specular, position), attenuation(attenuation) {}

	/// <summary>
	/// Calculate point light for scene object
	/// </summary>
	/// <param name="object">Scene object to calculate light</param>
	/// <param name="intersectionPoint">Intersection point of ray and object</param>
	/// <param name="cameraDirection">Direction vector of camera</param>
	/// <param name="shaded">Is object shaded (hidden by other)</param>
	/// <returns>Color of pixel after counting light properties</returns>
	LightIntensity calculateLight(SceneObject* object, Vector3 intersectionPoint, Vector3 cameraDirection, bool shaded);

	/// <summary>
	/// Point light max distance (based on attenuation)
	/// </summary>
	/// <returns>Value from W coordinate of attenuation vector4</returns>
	float getLightMaxDistance() { return attenuation.getW(); }

	/// <summary>
	/// Point light direction (from position to point)
	/// </summary>
	/// <param name="point">One of points of direction vector</param>
	/// <returns>One of point lights direction vector</returns>
	Vector3 getLightDirection(Vector3 point) { return (point - position).normalized(); }

private:
	/// <summary>
	/// PointLight attenuation: x- constant, y - linear, z - quadratic, w - maxDistance
	/// </summary>
	Vector4 attenuation;

};