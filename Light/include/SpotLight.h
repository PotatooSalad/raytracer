#pragma once
#include "Light.h"
#include "Vector4.h"

/// <summary>
/// Spot light class
/// </summary>
class SpotLight : public Light
{
public:
	/// <summary>
	/// Spot light constructor
	/// </summary>
	/// <param name="pos">Position of light</param>
	/// <param name="dir">Direction of light</param>
	/// <param name="cutOff">Cutoff angle (in degrees)</param>
	/// <param name="outerCutOff">Outer cutoff angle (in degrees)</param>
	/// <param name="att">Attenuation vector</param>
	/// <param name="amb">Ambient color of light</param>
	/// <param name="diff">Diffuse color of light</param>
	/// <param name="spec">Specular color of light</param>
	SpotLight(Vector3 pos, Vector3 dir, float cutOff, float outerCutOff, Vector4 att, LightIntensity amb, LightIntensity diff, LightIntensity spec)
		: Light(amb, diff, spec, pos), direction(dir), cutOff(cutOff), outerCutOff(outerCutOff), attenuation(att) {}

	/// <summary>
	/// Calculate light for scene object
	/// </summary>
	/// <param name="object">Scene object to calculate light</param>
	/// <param name="intersectionPoint">Intersection point of ray and object</param>
	/// <param name="cameraDirection">Direction vector of camera</param>
	/// <param name="shaded">Is object shaded (hidden by other)</param>
	/// <returns>Color of pixel after counting light properties</returns>
	LightIntensity calculateLight(SceneObject* object, Vector3 intersectionPoint, Vector3 cameraDirection, bool shaded);

	/// <summary>
	/// Returns maximum distance for light to search objects
	/// </summary>
	/// <returns>Maximum distance for ray intersection</returns>
	float getLightMaxDistance() { return attenuation.getW(); }

	/// <summary>
	/// Get spot light direction
	/// </summary>
	/// <param name="point">One of points of direction vector (if pointlight)</param>
	/// <returns>Direction of spot light</returns>
	Vector3 getLightDirection(Vector3 point) { return direction; }
private:


	/// <summary>
	/// Spot light direction
	/// </summary>
	Vector3 direction;

	/// <summary>
	/// Spot light cutoff angle (in degrees)
	/// </summary>
	float cutOff;

	/// <summary>
	/// Spot light outer cutoff angle (in degrees)
	/// </summary>
	float outerCutOff;

	/// <summary>
	/// Light attenuation
	/// </summary>
	Vector4 attenuation;
};

