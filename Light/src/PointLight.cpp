#include "PointLight.h"
#include "SceneObject.h"
#include "Material.h"
#include "LightIntensity.h"
#include "Hitable.h"

LightIntensity PointLight::calculateLight(SceneObject* object, Vector3 intersectionPoint, Vector3 cameraDirection, bool shaded)
{
	Vector3 lightDir = (position - intersectionPoint).normalized();

	Material* mat = object->getMaterial();
	LightIntensity matDiffuse = object->getObjectDiffuse(intersectionPoint);
	LightIntensity returnColor(0);

	if(!shaded)
	{
		Vector3 normal = object->getGeometry()->normalInPoint(intersectionPoint);

		normal.normalize();
		float diff = std::max(normal.dot(lightDir), 0.0f);
		Vector3 reflectDir = (-lightDir).reflect(normal);
		float spec = pow(std::max((-cameraDirection).dot(reflectDir), 0.0f), mat->getShineness());

		LightIntensity dif = diffuse * diff * matDiffuse;
		LightIntensity spe = specular * spec * mat->getSpecular();

		float distance = (position - intersectionPoint).getLength();
		float att = 1.0 / ((double)attenuation.getX() + (double)attenuation.getY() * distance + (double)attenuation.getZ() * ((double)distance * distance));
		dif *= att;
		spe *= att;
		returnColor = dif + spe;
	}
	return returnColor;
}