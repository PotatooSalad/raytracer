#include "SpotLight.h"
#include "SceneObject.h"
#include "Material.h"
#include "LightIntensity.h"
#include "Hitable.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>

LightIntensity SpotLight::calculateLight(SceneObject* object, Vector3 intersectionPoint, Vector3 cameraDirection, bool shaded)
{
	Vector3 lightDir = (position - intersectionPoint).normalized();

	Material* mat = object->getMaterial();

	LightIntensity matDiffuse = object->getObjectDiffuse(intersectionPoint);
	LightIntensity returnColor(0);
	float cutOffCos = cosf(cutOff * M_PI / 180.0f);
	float outerOffCos = cosf(outerCutOff * M_PI / 180.0f);

	if(!shaded)
	{
		Vector3 normal = object->getGeometry()->normalInPoint(intersectionPoint);

		normal.normalize();
		float diff = std::max(normal.dot(lightDir), 0.0f);
		Vector3 reflectDir = (-lightDir).reflect(normal);
		float spec = pow(std::max((-cameraDirection).dot(reflectDir), 0.0f), mat->getShineness());

		LightIntensity dif = diffuse * diff * matDiffuse;
		LightIntensity spe = specular * spec * mat->getSpecular();

		float distance = (position - intersectionPoint).getLength();
		float att = 1.0 / ((double)attenuation.getX() + (double)attenuation.getY() * distance + (double)attenuation.getZ() * ((double)distance * distance));

		float theta = lightDir.dot(-direction);
		float epsilon = cutOffCos - outerOffCos;
		float intensity = std::clamp((theta - outerOffCos) / epsilon, 0.0f, 1.0f);
		dif *= att * intensity;
		spe *= att * intensity;
		returnColor = dif + spe;
	}
	return returnColor;
}