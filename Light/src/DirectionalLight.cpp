#include "DirectionalLight.h"
#include "Scene.h"
#include "SceneObject.h"
#include "Hitable.h"
#include "Material.h"
#include "LightIntensity.h"

LightIntensity DirectionalLight::calculateLight(SceneObject* object, Vector3 intersectionPoint, Vector3 cameraDirection, bool shaded)
{
	Vector3 lightDir = (-direction).normalized();
	Material* mat = object->getMaterial();
	LightIntensity matDiffuse = object->getObjectDiffuse(intersectionPoint);

	LightIntensity returnColor(0);
	if(!shaded)
	{
		Vector3 normal = object->getGeometry()->normalInPoint(intersectionPoint).normalized();
		float diff = std::max(normal.dot(lightDir), 0.0f);
		Vector3 reflectDir = (-lightDir).reflect(normal);
		float spec = pow(std::max((-cameraDirection).dot(reflectDir), 0.0f), mat->getShineness());

		LightIntensity dif = diffuse * diff * matDiffuse;
		LightIntensity spe = specular * spec * mat->getSpecular();

		returnColor = dif + spe;
	}
	return returnColor;
}